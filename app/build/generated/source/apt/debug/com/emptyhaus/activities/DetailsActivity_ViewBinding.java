// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.ButtonRegular;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.fonts.TextViewMedium;
import com.emptyhaus.fonts.TextViewRegular;
import com.viewpagerindicator.CirclePageIndicator;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailsActivity_ViewBinding implements Unbinder {
  private DetailsActivity target;

  private View view2131230887;

  private View view2131230881;

  private View view2131230885;

  private View view2131231081;

  private View view2131231084;

  private View view2131230766;

  private View view2131230768;

  private View view2131230891;

  private View view2131230889;

  @UiThread
  public DetailsActivity_ViewBinding(DetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DetailsActivity_ViewBinding(final DetailsActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.ivRight, "field 'ivRight' and method 'onClick'");
    target.ivRight = Utils.castView(view, R.id.ivRight, "field 'ivRight'", ImageView.class);
    view2131230887 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivLeft, "field 'ivLeft' and method 'onClick'");
    target.ivLeft = Utils.castView(view, R.id.ivLeft, "field 'ivLeft'", ImageView.class);
    view2131230881 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.viewPager = Utils.findRequiredViewAsType(source, R.id.viewPager, "field 'viewPager'", ViewPager.class);
    target.indicator = Utils.findRequiredViewAsType(source, R.id.indicator, "field 'indicator'", CirclePageIndicator.class);
    target.rlUsa = Utils.findRequiredViewAsType(source, R.id.rlUsa, "field 'rlUsa'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.ivProfile, "field 'ivProfile' and method 'onClick'");
    target.ivProfile = Utils.castView(view, R.id.ivProfile, "field 'ivProfile'", ImageView.class);
    view2131230885 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.role = Utils.findRequiredViewAsType(source, R.id.role, "field 'role'", TextViewRegular.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextViewRegular.class);
    view = Utils.findRequiredView(source, R.id.tvRate, "field 'tvRate' and method 'onClick'");
    target.tvRate = Utils.castView(view, R.id.tvRate, "field 'tvRate'", TextViewRegular.class);
    view2131231081 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tvRequest, "field 'tvRequest' and method 'onClick'");
    target.tvRequest = Utils.castView(view, R.id.tvRequest, "field 'tvRequest'", TextViewRegular.class);
    view2131231084 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btnCall, "field 'btnCall' and method 'onClick'");
    target.btnCall = Utils.castView(view, R.id.btnCall, "field 'btnCall'", ButtonRegular.class);
    view2131230766 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvTittle = Utils.findRequiredViewAsType(source, R.id.tvTittle, "field 'tvTittle'", TextViewBold.class);
    target.tvLocality = Utils.findRequiredViewAsType(source, R.id.tvLocality, "field 'tvLocality'", TextViewRegular.class);
    target.tvArea = Utils.findRequiredViewAsType(source, R.id.tvArea, "field 'tvArea'", TextViewRegular.class);
    target.tvBedroom = Utils.findRequiredViewAsType(source, R.id.tvBedroom, "field 'tvBedroom'", TextViewRegular.class);
    target.tvBathroom = Utils.findRequiredViewAsType(source, R.id.tvBathroom, "field 'tvBathroom'", TextViewRegular.class);
    target.tvpropertyDescription = Utils.findRequiredViewAsType(source, R.id.tvpropertyDescription, "field 'tvpropertyDescription'", TextViewRegular.class);
    view = Utils.findRequiredView(source, R.id.btnSeeMore, "field 'btnSeeMore' and method 'onClick'");
    target.btnSeeMore = Utils.castView(view, R.id.btnSeeMore, "field 'btnSeeMore'", Button.class);
    view2131230768 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvPrice = Utils.findRequiredViewAsType(source, R.id.tvPrice, "field 'tvPrice'", TextViewMedium.class);
    view = Utils.findRequiredView(source, R.id.ivheartActive, "field 'ivheartActive' and method 'onClick'");
    target.ivheartActive = Utils.castView(view, R.id.ivheartActive, "field 'ivheartActive'", ImageView.class);
    view2131230891 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.pbfavourite = Utils.findRequiredViewAsType(source, R.id.pbfavourite, "field 'pbfavourite'", ProgressBar.class);
    view = Utils.findRequiredView(source, R.id.ivShare, "field 'ivShare' and method 'onClick'");
    target.ivShare = Utils.castView(view, R.id.ivShare, "field 'ivShare'", ImageView.class);
    view2131230889 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivRight = null;
    target.ivLeft = null;
    target.viewPager = null;
    target.indicator = null;
    target.rlUsa = null;
    target.ivProfile = null;
    target.role = null;
    target.tvName = null;
    target.tvRate = null;
    target.tvRequest = null;
    target.btnCall = null;
    target.tvTittle = null;
    target.tvLocality = null;
    target.tvArea = null;
    target.tvBedroom = null;
    target.tvBathroom = null;
    target.tvpropertyDescription = null;
    target.btnSeeMore = null;
    target.tvPrice = null;
    target.ivheartActive = null;
    target.pbfavourite = null;
    target.ivShare = null;

    view2131230887.setOnClickListener(null);
    view2131230887 = null;
    view2131230881.setOnClickListener(null);
    view2131230881 = null;
    view2131230885.setOnClickListener(null);
    view2131230885 = null;
    view2131231081.setOnClickListener(null);
    view2131231081 = null;
    view2131231084.setOnClickListener(null);
    view2131231084 = null;
    view2131230766.setOnClickListener(null);
    view2131230766 = null;
    view2131230768.setOnClickListener(null);
    view2131230768 = null;
    view2131230891.setOnClickListener(null);
    view2131230891 = null;
    view2131230889.setOnClickListener(null);
    view2131230889 = null;
  }
}
