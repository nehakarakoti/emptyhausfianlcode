// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CitiesSelectionActivity_ViewBinding implements Unbinder {
  private CitiesSelectionActivity target;

  private View view2131230871;

  private View view2131231078;

  private View view2131231079;

  private View view2131230882;

  @UiThread
  public CitiesSelectionActivity_ViewBinding(CitiesSelectionActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CitiesSelectionActivity_ViewBinding(final CitiesSelectionActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.ivBack, "field 'ivBack' and method 'onClick'");
    target.ivBack = Utils.castView(view, R.id.ivBack, "field 'ivBack'", ImageView.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvEnterEmail = Utils.findRequiredViewAsType(source, R.id.tvEnterEmail, "field 'tvEnterEmail'", TextViewBold.class);
    view = Utils.findRequiredView(source, R.id.tvPopuPSelected, "field 'tvPopuPSelected' and method 'onClick'");
    target.tvPopuPSelected = Utils.castView(view, R.id.tvPopuPSelected, "field 'tvPopuPSelected'", TextViewBold.class);
    view2131231078 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.ivDropDown = Utils.findRequiredViewAsType(source, R.id.ivDropDown, "field 'ivDropDown'", ImageView.class);
    target.rlDropDown = Utils.findRequiredViewAsType(source, R.id.rlDropDown, "field 'rlDropDown'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.tvPopuPSelected2, "field 'tvPopuPSelected2' and method 'onClick'");
    target.tvPopuPSelected2 = Utils.castView(view, R.id.tvPopuPSelected2, "field 'tvPopuPSelected2'", TextViewBold.class);
    view2131231079 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.ivDropDown2 = Utils.findRequiredViewAsType(source, R.id.ivDropDown2, "field 'ivDropDown2'", ImageView.class);
    target.rlDropDown2 = Utils.findRequiredViewAsType(source, R.id.rlDropDown2, "field 'rlDropDown2'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.ivNext, "field 'ivNext' and method 'onClick'");
    target.ivNext = Utils.castView(view, R.id.ivNext, "field 'ivNext'", ImageView.class);
    view2131230882 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.pbCity1 = Utils.findRequiredViewAsType(source, R.id.pbCity1, "field 'pbCity1'", ProgressBar.class);
    target.pbCity2 = Utils.findRequiredViewAsType(source, R.id.pbCity2, "field 'pbCity2'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CitiesSelectionActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivBack = null;
    target.tvEnterEmail = null;
    target.tvPopuPSelected = null;
    target.ivDropDown = null;
    target.rlDropDown = null;
    target.tvPopuPSelected2 = null;
    target.ivDropDown2 = null;
    target.rlDropDown2 = null;
    target.ivNext = null;
    target.pbCity1 = null;
    target.pbCity2 = null;

    view2131230871.setOnClickListener(null);
    view2131230871 = null;
    view2131231078.setOnClickListener(null);
    view2131231078 = null;
    view2131231079.setOnClickListener(null);
    view2131231079 = null;
    view2131230882.setOnClickListener(null);
    view2131230882 = null;
  }
}
