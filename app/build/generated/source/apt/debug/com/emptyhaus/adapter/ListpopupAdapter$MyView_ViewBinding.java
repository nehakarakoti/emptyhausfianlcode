// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListpopupAdapter$MyView_ViewBinding implements Unbinder {
  private ListpopupAdapter.MyView target;

  private View view2131231074;

  @UiThread
  public ListpopupAdapter$MyView_ViewBinding(final ListpopupAdapter.MyView target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.tvName, "field 'tvName' and method 'onClick'");
    target.tvName = Utils.castView(view, R.id.tvName, "field 'tvName'", TextView.class);
    view2131231074 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ListpopupAdapter.MyView target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;

    view2131231074.setOnClickListener(null);
    view2131231074 = null;
  }
}
