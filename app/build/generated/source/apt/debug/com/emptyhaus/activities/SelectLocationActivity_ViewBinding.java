// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectLocationActivity_ViewBinding implements Unbinder {
  private SelectLocationActivity target;

  private View view2131230871;

  private View view2131230973;

  private View view2131231078;

  private View view2131230882;

  @UiThread
  public SelectLocationActivity_ViewBinding(SelectLocationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SelectLocationActivity_ViewBinding(final SelectLocationActivity target, View source) {
    this.target = target;

    View view;
    target.recyclerview = Utils.findRequiredViewAsType(source, R.id.recyclerview, "field 'recyclerview'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.ivBack, "field 'ivBack' and method 'onClick'");
    target.ivBack = Utils.castView(view, R.id.ivBack, "field 'ivBack'", ImageView.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rlDropDown, "field 'rlDropDown' and method 'onClick'");
    target.rlDropDown = Utils.castView(view, R.id.rlDropDown, "field 'rlDropDown'", RelativeLayout.class);
    view2131230973 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tvPopuPSelected, "field 'tvPopuPSelected' and method 'onClick'");
    target.tvPopuPSelected = Utils.castView(view, R.id.tvPopuPSelected, "field 'tvPopuPSelected'", TextView.class);
    view2131231078 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivNext, "field 'ivNext' and method 'onClick'");
    target.ivNext = Utils.castView(view, R.id.ivNext, "field 'ivNext'", ImageView.class);
    view2131230882 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvEnterEmail = Utils.findRequiredViewAsType(source, R.id.tvEnterEmail, "field 'tvEnterEmail'", TextViewBold.class);
    target.pbBar = Utils.findRequiredViewAsType(source, R.id.pbBar, "field 'pbBar'", ProgressBar.class);
    target.ivDropDown = Utils.findRequiredViewAsType(source, R.id.ivDropDown, "field 'ivDropDown'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SelectLocationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerview = null;
    target.ivBack = null;
    target.rlDropDown = null;
    target.tvPopuPSelected = null;
    target.ivNext = null;
    target.tvEnterEmail = null;
    target.pbBar = null;
    target.ivDropDown = null;

    view2131230871.setOnClickListener(null);
    view2131230871 = null;
    view2131230973.setOnClickListener(null);
    view2131230973 = null;
    view2131231078.setOnClickListener(null);
    view2131231078 = null;
    view2131230882.setOnClickListener(null);
    view2131230882 = null;
  }
}
