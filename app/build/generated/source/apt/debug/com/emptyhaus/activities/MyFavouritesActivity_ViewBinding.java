// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.ButtonMedium;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.fonts.TextViewMedium;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyFavouritesActivity_ViewBinding implements Unbinder {
  private MyFavouritesActivity target;

  private View view2131230766;

  @UiThread
  public MyFavouritesActivity_ViewBinding(MyFavouritesActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MyFavouritesActivity_ViewBinding(final MyFavouritesActivity target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerview, "field 'recyclerView'", RecyclerView.class);
    target.tvNodata = Utils.findRequiredViewAsType(source, R.id.tvNodata, "field 'tvNodata'", TextView.class);
    target.tvFav = Utils.findRequiredViewAsType(source, R.id.tvFav, "field 'tvFav'", TextViewBold.class);
    target.tv = Utils.findRequiredViewAsType(source, R.id.tv, "field 'tv'", TextViewMedium.class);
    view = Utils.findRequiredView(source, R.id.btnCall, "field 'btnCall' and method 'onClick'");
    target.btnCall = Utils.castView(view, R.id.btnCall, "field 'btnCall'", ButtonMedium.class);
    view2131230766 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
    target.ll = Utils.findRequiredViewAsType(source, R.id.ll, "field 'll'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MyFavouritesActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.tvNodata = null;
    target.tvFav = null;
    target.tv = null;
    target.btnCall = null;
    target.ll = null;

    view2131230766.setOnClickListener(null);
    view2131230766 = null;
  }
}
