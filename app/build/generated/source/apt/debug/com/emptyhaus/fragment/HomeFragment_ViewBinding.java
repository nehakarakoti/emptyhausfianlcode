// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeFragment_ViewBinding implements Unbinder {
  private HomeFragment target;

  private View view2131231071;

  private View view2131231070;

  @UiThread
  public HomeFragment_ViewBinding(final HomeFragment target, View source) {
    this.target = target;

    View view;
    target.toolbarLayout = Utils.findRequiredViewAsType(source, R.id.toolbar_layout, "field 'toolbarLayout'", CollapsingToolbarLayout.class);
    view = Utils.findRequiredView(source, R.id.tvForsale, "field 'tvForsale' and method 'onClick'");
    target.tvForsale = Utils.castView(view, R.id.tvForsale, "field 'tvForsale'", TextViewRegular.class);
    view2131231071 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tvForRent, "field 'tvForRent' and method 'onClick'");
    target.tvForRent = Utils.castView(view, R.id.tvForRent, "field 'tvForRent'", TextViewRegular.class);
    view2131231070 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.searchBar = Utils.findRequiredViewAsType(source, R.id.searchBar, "field 'searchBar'", LinearLayout.class);
    target.recyclerview = Utils.findRequiredViewAsType(source, R.id.recyclerview, "field 'recyclerview'", RecyclerView.class);
    target.appBar = Utils.findRequiredViewAsType(source, R.id.app_bar, "field 'appBar'", AppBarLayout.class);
    target.etSearchLocation = Utils.findRequiredViewAsType(source, R.id.etSearchLocation, "field 'etSearchLocation'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HomeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbarLayout = null;
    target.tvForsale = null;
    target.tvForRent = null;
    target.searchBar = null;
    target.recyclerview = null;
    target.appBar = null;
    target.etSearchLocation = null;

    view2131231071.setOnClickListener(null);
    view2131231071 = null;
    view2131231070.setOnClickListener(null);
    view2131231070 = null;
  }
}
