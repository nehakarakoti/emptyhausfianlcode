// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.ButtonMedium;
import com.emptyhaus.fonts.TextViewRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CallingDetailsActivity_ViewBinding implements Unbinder {
  private CallingDetailsActivity target;

  private View view2131230871;

  private View view2131230766;

  private View view2131231082;

  private View view2131231083;

  private View view2131231085;

  @UiThread
  public CallingDetailsActivity_ViewBinding(CallingDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CallingDetailsActivity_ViewBinding(final CallingDetailsActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.ivBack, "field 'ivBack' and method 'onClick'");
    target.ivBack = Utils.castView(view, R.id.ivBack, "field 'ivBack'", ImageView.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvRate = Utils.findRequiredViewAsType(source, R.id.tvRate, "field 'tvRate'", TextView.class);
    target.tvRequest = Utils.findRequiredViewAsType(source, R.id.tvRequest, "field 'tvRequest'", TextView.class);
    target.ivImage = Utils.findRequiredViewAsType(source, R.id.ivImage, "field 'ivImage'", ImageView.class);
    target.tvBedRooms = Utils.findRequiredViewAsType(source, R.id.tvBedRooms, "field 'tvBedRooms'", TextView.class);
    target.ivBed = Utils.findRequiredViewAsType(source, R.id.ivBed, "field 'ivBed'", ImageView.class);
    target.tvBathrroom = Utils.findRequiredViewAsType(source, R.id.tvBathrroom, "field 'tvBathrroom'", TextView.class);
    target.ivvBath = Utils.findRequiredViewAsType(source, R.id.ivvBath, "field 'ivvBath'", ImageView.class);
    target.tv = Utils.findRequiredViewAsType(source, R.id.tv, "field 'tv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btnCall, "field 'btnCall' and method 'onClick'");
    target.btnCall = Utils.castView(view, R.id.btnCall, "field 'btnCall'", ButtonMedium.class);
    view2131230766 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tvRatings, "field 'tvRatings' and method 'onClick'");
    target.tvRatings = Utils.castView(view, R.id.tvRatings, "field 'tvRatings'", TextView.class);
    view2131231082 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tvRent, "field 'tvRent' and method 'onClick'");
    target.tvRent = Utils.castView(view, R.id.tvRent, "field 'tvRent'", TextView.class);
    view2131231083 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tvSale, "field 'tvSale' and method 'onClick'");
    target.tvSale = Utils.castView(view, R.id.tvSale, "field 'tvSale'", TextView.class);
    view2131231085 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.ivImage1 = Utils.findRequiredViewAsType(source, R.id.ivImage1, "field 'ivImage1'", ImageView.class);
    target.tvBedRoom1s = Utils.findRequiredViewAsType(source, R.id.tvBedRoom1s, "field 'tvBedRoom1s'", TextViewRegular.class);
    target.ivBed1 = Utils.findRequiredViewAsType(source, R.id.ivBed1, "field 'ivBed1'", ImageView.class);
    target.tvBathrroom1 = Utils.findRequiredViewAsType(source, R.id.tvBathrroom1, "field 'tvBathrroom1'", TextViewRegular.class);
    target.ivvBath1 = Utils.findRequiredViewAsType(source, R.id.ivvBath1, "field 'ivvBath1'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CallingDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivBack = null;
    target.tvRate = null;
    target.tvRequest = null;
    target.ivImage = null;
    target.tvBedRooms = null;
    target.ivBed = null;
    target.tvBathrroom = null;
    target.ivvBath = null;
    target.tv = null;
    target.btnCall = null;
    target.tvRatings = null;
    target.tvRent = null;
    target.tvSale = null;
    target.ivImage1 = null;
    target.tvBedRoom1s = null;
    target.ivBed1 = null;
    target.tvBathrroom1 = null;
    target.ivvBath1 = null;

    view2131230871.setOnClickListener(null);
    view2131230871 = null;
    view2131230766.setOnClickListener(null);
    view2131230766 = null;
    view2131231082.setOnClickListener(null);
    view2131231082 = null;
    view2131231083.setOnClickListener(null);
    view2131231083 = null;
    view2131231085.setOnClickListener(null);
    view2131231085 = null;
  }
}
