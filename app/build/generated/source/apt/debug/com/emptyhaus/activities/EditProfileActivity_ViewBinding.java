// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.EditTextRegular;
import com.emptyhaus.fonts.TextViewMedium;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditProfileActivity_ViewBinding implements Unbinder {
  private EditProfileActivity target;

  private View view2131230871;

  private View view2131230816;

  private View view2131230817;

  private View view2131230818;

  private View view2131230883;

  private View view2131231067;

  private View view2131230821;

  private View view2131230820;

  @UiThread
  public EditProfileActivity_ViewBinding(EditProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EditProfileActivity_ViewBinding(final EditProfileActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.ivBack, "field 'ivBack' and method 'onClick'");
    target.ivBack = Utils.castView(view, R.id.ivBack, "field 'ivBack'", ImageView.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.rlToolbar = Utils.findRequiredViewAsType(source, R.id.rlToolbar, "field 'rlToolbar'", RelativeLayout.class);
    target.etFullName = Utils.findRequiredViewAsType(source, R.id.etFullName, "field 'etFullName'", EditText.class);
    view = Utils.findRequiredView(source, R.id.edit1, "field 'edit1' and method 'onClick'");
    target.edit1 = Utils.castView(view, R.id.edit1, "field 'edit1'", ImageView.class);
    view2131230816 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.etEmail, "field 'etEmail'", EditText.class);
    view = Utils.findRequiredView(source, R.id.edit2, "field 'edit2' and method 'onClick'");
    target.edit2 = Utils.castView(view, R.id.edit2, "field 'edit2'", ImageView.class);
    view2131230817 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.etPassword, "field 'etPassword'", EditText.class);
    view = Utils.findRequiredView(source, R.id.edit3, "field 'edit3' and method 'onClick'");
    target.edit3 = Utils.castView(view, R.id.edit3, "field 'edit3'", ImageView.class);
    view2131230818 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivPhotoUpload, "field 'ivPhotoUpload' and method 'onClick'");
    target.ivPhotoUpload = Utils.castView(view, R.id.ivPhotoUpload, "field 'ivPhotoUpload'", ImageView.class);
    view2131230883 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.et2 = Utils.findRequiredViewAsType(source, R.id.et2, "field 'et2'", EditTextRegular.class);
    target.et3 = Utils.findRequiredViewAsType(source, R.id.et3, "field 'et3'", EditTextRegular.class);
    target.ivProfilePhoto = Utils.findRequiredViewAsType(source, R.id.ivProfilePhoto, "field 'ivProfilePhoto'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.tvChangeLocation, "field 'tvChangeLocation' and method 'onClick'");
    target.tvChangeLocation = Utils.castView(view, R.id.tvChangeLocation, "field 'tvChangeLocation'", TextViewMedium.class);
    view2131231067 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.pbEdit1 = Utils.findRequiredViewAsType(source, R.id.pbEdit1, "field 'pbEdit1'", ProgressBar.class);
    target.pbEdit2 = Utils.findRequiredViewAsType(source, R.id.pbEdit2, "field 'pbEdit2'", ProgressBar.class);
    target.pbEditmin = Utils.findRequiredViewAsType(source, R.id.pbEditmin, "field 'pbEditmin'", ProgressBar.class);
    target.pbEditmax = Utils.findRequiredViewAsType(source, R.id.pbEditmax, "field 'pbEditmax'", ProgressBar.class);
    view = Utils.findRequiredView(source, R.id.editmin, "field 'editmin' and method 'onClick'");
    target.editmin = Utils.castView(view, R.id.editmin, "field 'editmin'", ImageView.class);
    view2131230821 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.editmax, "field 'editmax' and method 'onClick'");
    target.editmax = Utils.castView(view, R.id.editmax, "field 'editmax'", ImageView.class);
    view2131230820 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EditProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivBack = null;
    target.rlToolbar = null;
    target.etFullName = null;
    target.edit1 = null;
    target.etEmail = null;
    target.edit2 = null;
    target.etPassword = null;
    target.edit3 = null;
    target.ivPhotoUpload = null;
    target.et2 = null;
    target.et3 = null;
    target.ivProfilePhoto = null;
    target.tvChangeLocation = null;
    target.pbEdit1 = null;
    target.pbEdit2 = null;
    target.pbEditmin = null;
    target.pbEditmax = null;
    target.editmin = null;
    target.editmax = null;
    target.tvName = null;

    view2131230871.setOnClickListener(null);
    view2131230871 = null;
    view2131230816.setOnClickListener(null);
    view2131230816 = null;
    view2131230817.setOnClickListener(null);
    view2131230817 = null;
    view2131230818.setOnClickListener(null);
    view2131230818 = null;
    view2131230883.setOnClickListener(null);
    view2131230883 = null;
    view2131231067.setOnClickListener(null);
    view2131231067 = null;
    view2131230821.setOnClickListener(null);
    view2131230821 = null;
    view2131230820.setOnClickListener(null);
    view2131230820 = null;
  }
}
