// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpdateLocationActivity_ViewBinding implements Unbinder {
  private UpdateLocationActivity target;

  private View view2131230871;

  private View view2131230882;

  @UiThread
  public UpdateLocationActivity_ViewBinding(UpdateLocationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public UpdateLocationActivity_ViewBinding(final UpdateLocationActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.ivBack, "field 'ivBack' and method 'onClick'");
    target.ivBack = Utils.castView(view, R.id.ivBack, "field 'ivBack'", ImageView.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvEnterEmail = Utils.findRequiredViewAsType(source, R.id.tvEnterEmail, "field 'tvEnterEmail'", TextViewBold.class);
    target.tvPopuPSelected = Utils.findRequiredViewAsType(source, R.id.tvPopuPSelected, "field 'tvPopuPSelected'", TextViewBold.class);
    target.ivDropDown = Utils.findRequiredViewAsType(source, R.id.ivDropDown, "field 'ivDropDown'", ImageView.class);
    target.rlDropDown = Utils.findRequiredViewAsType(source, R.id.rlDropDown, "field 'rlDropDown'", RelativeLayout.class);
    target.recyclerview = Utils.findRequiredViewAsType(source, R.id.recyclerview, "field 'recyclerview'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.ivNext, "field 'ivNext' and method 'onClick'");
    target.ivNext = Utils.castView(view, R.id.ivNext, "field 'ivNext'", ImageView.class);
    view2131230882 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.pbCities = Utils.findRequiredViewAsType(source, R.id.pbCities, "field 'pbCities'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UpdateLocationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivBack = null;
    target.tvEnterEmail = null;
    target.tvPopuPSelected = null;
    target.ivDropDown = null;
    target.rlDropDown = null;
    target.recyclerview = null;
    target.ivNext = null;
    target.pbCities = null;

    view2131230871.setOnClickListener(null);
    view2131230871 = null;
    view2131230882.setOnClickListener(null);
    view2131230882 = null;
  }
}
