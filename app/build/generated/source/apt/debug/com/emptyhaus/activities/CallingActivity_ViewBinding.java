// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CallingActivity_ViewBinding implements Unbinder {
  private CallingActivity target;

  private View view2131230845;

  @UiThread
  public CallingActivity_ViewBinding(CallingActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CallingActivity_ViewBinding(final CallingActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.flCalling, "field 'flCalling' and method 'onClick'");
    target.flCalling = Utils.castView(view, R.id.flCalling, "field 'flCalling'", FrameLayout.class);
    view2131230845 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CallingActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.flCalling = null;

    view2131230845.setOnClickListener(null);
    view2131230845 = null;
  }
}
