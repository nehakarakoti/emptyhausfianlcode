// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewMedium;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view2131230912;

  private View view2131230888;

  private View view2131230890;

  private View view2131230908;

  private View view2131230911;

  private View view2131230914;

  private View view2131230905;

  private View view2131230771;

  private View view2131230965;

  private View view2131230913;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.llShortListed, "field 'llShortListed' and method 'onClick'");
    target.llShortListed = Utils.castView(view, R.id.llShortListed, "field 'llShortListed'", LinearLayout.class);
    view2131230912 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivSetting, "field 'ivSetting' and method 'onClick'");
    target.ivSetting = Utils.castView(view, R.id.ivSetting, "field 'ivSetting'", ImageView.class);
    view2131230888 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivUser, "field 'ivUser' and method 'onClick'");
    target.ivUser = Utils.castView(view, R.id.ivUser, "field 'ivUser'", ImageView.class);
    view2131230890 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.imageView = Utils.findRequiredViewAsType(source, R.id.imageView, "field 'imageView'", ImageView.class);
    target.tvShort = Utils.findRequiredViewAsType(source, R.id.tvShort, "field 'tvShort'", TextViewMedium.class);
    target.navView = Utils.findRequiredViewAsType(source, R.id.nav_view, "field 'navView'", NavigationView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.container = Utils.findRequiredViewAsType(source, R.id.container, "field 'container'", FrameLayout.class);
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawerLayout'", DrawerLayout.class);
    view = Utils.findRequiredView(source, R.id.llHome_nav, "field 'llHomeNav' and method 'onClick'");
    target.llHomeNav = Utils.castView(view, R.id.llHome_nav, "field 'llHomeNav'", LinearLayout.class);
    view2131230908 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llSetting, "field 'llSetting' and method 'onClick'");
    target.llSetting = Utils.castView(view, R.id.llSetting, "field 'llSetting'", LinearLayout.class);
    view2131230911 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llTerms, "field 'llTerms' and method 'onClick'");
    target.llTerms = Utils.castView(view, R.id.llTerms, "field 'llTerms'", LinearLayout.class);
    view2131230914 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llAbout, "field 'llAbout' and method 'onClick'");
    target.llAbout = Utils.castView(view, R.id.llAbout, "field 'llAbout'", LinearLayout.class);
    view2131230905 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.call, "field 'call' and method 'onClick'");
    target.call = Utils.castView(view, R.id.call, "field 'call'", LinearLayout.class);
    view2131230771 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rate, "field 'rate' and method 'onClick'");
    target.rate = Utils.castView(view, R.id.rate, "field 'rate'", LinearLayout.class);
    view2131230965 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llSignOut, "field 'llSignOut' and method 'onClick'");
    target.llSignOut = Utils.castView(view, R.id.llSignOut, "field 'llSignOut'", LinearLayout.class);
    view2131230913 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvUserName = Utils.findRequiredViewAsType(source, R.id.tvUserName, "field 'tvUserName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.llShortListed = null;
    target.ivSetting = null;
    target.ivUser = null;
    target.imageView = null;
    target.tvShort = null;
    target.navView = null;
    target.toolbar = null;
    target.container = null;
    target.drawerLayout = null;
    target.llHomeNav = null;
    target.llSetting = null;
    target.llTerms = null;
    target.llAbout = null;
    target.call = null;
    target.rate = null;
    target.llSignOut = null;
    target.tvUserName = null;

    view2131230912.setOnClickListener(null);
    view2131230912 = null;
    view2131230888.setOnClickListener(null);
    view2131230888 = null;
    view2131230890.setOnClickListener(null);
    view2131230890 = null;
    view2131230908.setOnClickListener(null);
    view2131230908 = null;
    view2131230911.setOnClickListener(null);
    view2131230911 = null;
    view2131230914.setOnClickListener(null);
    view2131230914 = null;
    view2131230905.setOnClickListener(null);
    view2131230905 = null;
    view2131230771.setOnClickListener(null);
    view2131230771 = null;
    view2131230965.setOnClickListener(null);
    view2131230965 = null;
    view2131230913.setOnClickListener(null);
    view2131230913 = null;
  }
}
