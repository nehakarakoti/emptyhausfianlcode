// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.facebook.login.widget.LoginButton;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  private View view2131231091;

  private View view2131231073;

  private View view2131230877;

  private View view2131230976;

  private View view2131230975;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(final LoginActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.txtCreateNewACTV, "field 'txtCreateNewACTV' and method 'onClick'");
    target.txtCreateNewACTV = Utils.castView(view, R.id.txtCreateNewACTV, "field 'txtCreateNewACTV'", TextView.class);
    view2131231091 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tvLogin, "field 'tvLogin' and method 'onClick'");
    target.tvLogin = Utils.castView(view, R.id.tvLogin, "field 'tvLogin'", TextView.class);
    view2131231073 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivGoogle, "field 'ivGoogle' and method 'onClick'");
    target.ivGoogle = Utils.castView(view, R.id.ivGoogle, "field 'ivGoogle'", ImageView.class);
    view2131230877 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rlGoogle, "field 'rlGoogle' and method 'onClick'");
    target.rlGoogle = Utils.castView(view, R.id.rlGoogle, "field 'rlGoogle'", RelativeLayout.class);
    view2131230976 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rlFacebook, "field 'rlFacebook' and method 'onClick'");
    target.rlFacebook = Utils.castView(view, R.id.rlFacebook, "field 'rlFacebook'", RelativeLayout.class);
    view2131230975 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.fb_origin_login_button = Utils.findRequiredViewAsType(source, R.id.login_button, "field 'fb_origin_login_button'", LoginButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtCreateNewACTV = null;
    target.tvLogin = null;
    target.ivGoogle = null;
    target.rlGoogle = null;
    target.rlFacebook = null;
    target.fb_origin_login_button = null;

    view2131231091.setOnClickListener(null);
    view2131231091 = null;
    view2131231073.setOnClickListener(null);
    view2131231073 = null;
    view2131230877.setOnClickListener(null);
    view2131230877 = null;
    view2131230976.setOnClickListener(null);
    view2131230976 = null;
    view2131230975.setOnClickListener(null);
    view2131230975 = null;
  }
}
