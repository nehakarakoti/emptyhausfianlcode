// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.EditTextRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EnterPasswordActivity_ViewBinding implements Unbinder {
  private EnterPasswordActivity target;

  private View view2131230871;

  private View view2131230882;

  @UiThread
  public EnterPasswordActivity_ViewBinding(EnterPasswordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EnterPasswordActivity_ViewBinding(final EnterPasswordActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.ivBack, "field 'ivBack' and method 'onClick'");
    target.ivBack = Utils.castView(view, R.id.ivBack, "field 'ivBack'", ImageView.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ivNext, "field 'ivNext' and method 'onClick'");
    target.ivNext = Utils.castView(view, R.id.ivNext, "field 'ivNext'", ImageView.class);
    view2131230882 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.etPassword, "field 'etPassword'", EditTextRegular.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EnterPasswordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivBack = null;
    target.ivNext = null;
    target.etPassword = null;

    view2131230871.setOnClickListener(null);
    view2131230871 = null;
    view2131230882.setOnClickListener(null);
    view2131230882 = null;
  }
}
