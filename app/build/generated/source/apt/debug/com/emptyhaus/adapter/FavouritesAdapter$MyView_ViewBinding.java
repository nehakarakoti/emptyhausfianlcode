// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewMedium;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FavouritesAdapter$MyView_ViewBinding implements Unbinder {
  private FavouritesAdapter.MyView target;

  @UiThread
  public FavouritesAdapter$MyView_ViewBinding(FavouritesAdapter.MyView target, View source) {
    this.target = target;

    target.ivImage = Utils.findRequiredViewAsType(source, R.id.ivImage, "field 'ivImage'", ImageView.class);
    target.tvBedRooms = Utils.findRequiredViewAsType(source, R.id.tvBedRooms, "field 'tvBedRooms'", TextViewMedium.class);
    target.ivBed = Utils.findRequiredViewAsType(source, R.id.ivBed, "field 'ivBed'", ImageView.class);
    target.tvBathrroom = Utils.findRequiredViewAsType(source, R.id.tvBathrroom, "field 'tvBathrroom'", TextViewMedium.class);
    target.ivvBath = Utils.findRequiredViewAsType(source, R.id.ivvBath, "field 'ivvBath'", ImageView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tvAddress, "field 'tvAddress'", TextView.class);
    target.ivPrice = Utils.findRequiredViewAsType(source, R.id.ivPrice, "field 'ivPrice'", LinearLayout.class);
    target.tvPrice = Utils.findRequiredViewAsType(source, R.id.tvPrice, "field 'tvPrice'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FavouritesAdapter.MyView target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivImage = null;
    target.tvBedRooms = null;
    target.ivBed = null;
    target.tvBathrroom = null;
    target.ivvBath = null;
    target.tvAddress = null;
    target.ivPrice = null;
    target.tvPrice = null;
  }
}
