// Generated code from Butter Knife. Do not modify!
package com.emptyhaus.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ItemAdapter$MyView_ViewBinding implements Unbinder {
  private ItemAdapter.MyView target;

  private View view2131230977;

  private View view2131230889;

  @UiThread
  public ItemAdapter$MyView_ViewBinding(final ItemAdapter.MyView target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.rlPhoto, "field 'rlPhoto' and method 'onClick'");
    target.rlPhoto = Utils.castView(view, R.id.rlPhoto, "field 'rlPhoto'", RelativeLayout.class);
    view2131230977 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.ivHouse = Utils.findRequiredViewAsType(source, R.id.ivHouse, "field 'ivHouse'", ImageView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'tvName'", TextViewBold.class);
    view = Utils.findRequiredView(source, R.id.ivShare, "field 'ivShare' and method 'onClick'");
    target.ivShare = Utils.castView(view, R.id.ivShare, "field 'ivShare'", ImageView.class);
    view2131230889 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.tvArea = Utils.findRequiredViewAsType(source, R.id.tvArea, "field 'tvArea'", TextView.class);
    target.tvBedroom = Utils.findRequiredViewAsType(source, R.id.tvBedroom, "field 'tvBedroom'", TextView.class);
    target.tvBathroom = Utils.findRequiredViewAsType(source, R.id.tvBathroom, "field 'tvBathroom'", TextView.class);
    target.locality = Utils.findRequiredViewAsType(source, R.id.locality, "field 'locality'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ItemAdapter.MyView target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rlPhoto = null;
    target.ivHouse = null;
    target.tvName = null;
    target.ivShare = null;
    target.tvArea = null;
    target.tvBedroom = null;
    target.tvBathroom = null;
    target.locality = null;

    view2131230977.setOnClickListener(null);
    view2131230977 = null;
    view2131230889.setOnClickListener(null);
    view2131230889 = null;
  }
}
