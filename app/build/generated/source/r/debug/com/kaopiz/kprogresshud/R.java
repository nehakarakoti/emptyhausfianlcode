/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.kaopiz.kprogresshud;

public final class R {
    public static final class color {
        public static final int kprogresshud_default_color = 0x7f05007d;
        public static final int kprogresshud_grey_color = 0x7f05007e;
    }
    public static final class drawable {
        public static final int kprogresshud_spinner = 0x7f0700cf;
    }
    public static final class id {
        public static final int background = 0x7f080027;
        public static final int container = 0x7f08004b;
        public static final int details_label = 0x7f08005a;
        public static final int label = 0x7f0800ae;
    }
    public static final class layout {
        public static final int kprogresshud_hud = 0x7f0b0052;
    }
}
