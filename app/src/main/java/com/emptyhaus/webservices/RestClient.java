package com.emptyhaus.webservices;

import com.emptyhaus.utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static ApiInterface REST_CLIENT;
    private static Retrofit retrofit;


    static {
        setupRestClient();
    }
    public static ApiInterface get() {
        return REST_CLIENT;
    }
    private RestClient() {
    }

    private static void setupRestClient() {

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        REST_CLIENT = retrofit.create(ApiInterface.class);

    }


    private static OkHttpClient getOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(500, TimeUnit.SECONDS);
        httpClient.readTimeout(500, TimeUnit.SECONDS);
        // add your other interceptors …
//        httpClient.addInterceptor(new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
////                String sessionId= ApplicationGlobal.prefsManager.getSessionId();
////                Request request = chain.request().newBuilder().addHeader("Authorization", sessionId).build();
//                return chain.proceed(request);
//            }
//        });
// add logging as last interceptor
        httpClient.addNetworkInterceptor(logging);

        return httpClient.build();
    }

    public static Retrofit getRetrofitInstance() {
        return retrofit;
    }
}
