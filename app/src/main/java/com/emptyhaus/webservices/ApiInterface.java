package com.emptyhaus.webservices;

import com.emptyhaus.model.CurrentLocation;
import com.emptyhaus.model.FavList;
import com.emptyhaus.model.Favourite;
import com.emptyhaus.model.Home;
import com.emptyhaus.model.ItemDetails;
import com.emptyhaus.model.SignUp;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("signup.php")
    Call<SignUp> signUp(@Field("firstname") String firstname, @Field("lastname") String lastname, @Field("email") String email, @Field("password") String password,
                        @Field("min_budget") String min_budget, @Field("max_budget") String max_budget,
                        @Field("location") String location
            , @Field("city1") String city1, @Field("city2") String city2);


    @GET("GetAllLocations.php")
    Call<CurrentLocation> getLocations();

@GET("GetLocationCities.php")
Call<CurrentLocation>getSubLocation(@Query("location_id") String location_id);


    @FormUrlEncoded
    @POST("GetPropertyById.php")
    Call<ItemDetails> gettingItemDeatils(@Field("property_id")String property_id,@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("login.php")
    Call<SignUp> logIn(@Field("email") String email, @Field("password") String password);

    @GET("GetUserDetail.php")
    Call<SignUp> gettingDetails(@Query("id") String id);

    @GET("GetAllProperties.php")
    Call<Home> gettingHomeData(@Query("property_type") String property_type ,@Query("user_id") String user_id);

    @FormUrlEncoded
    @POST("UpdateMinBudget.php")
    Call<SignUp> updateMin(@Field("id") String id, @Field("min_budget") String min_budget);

    @FormUrlEncoded
    @POST("AddPropertyToFav.php")
    Call<Favourite> addPropertyToFavourites(@Field("user_id") String user_id, @Field("property_id") String property_id);


    @FormUrlEncoded
    @POST("UpdateUserName.php")
    Call<SignUp> updatingUserName(@Field("id") String id, @Field("name") String name);

    @GET("GetAllFavProperties.php")
    Call<FavList> favListComing(@Query("user_id") String user_id);


    @FormUrlEncoded
    @POST("UpdateUserPassword.php")
    Call<SignUp> updatingPassword(@Field("id") String id, @Field("password") String password);

    @FormUrlEncoded
    @POST("UpdateUserLocation.php")
    Call<SignUp> updatingLocation(@Field("id") String id, @Field("location") String location);
//@GET("GetPropertyById.php")
//Call<>


    @FormUrlEncoded
    @POST("UpdateMaxBudget.php")
    Call<SignUp> maxBudget(@Field("id") String id, @Field("max_budget") String min_budget);
}
