package com.emptyhaus.activities;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emptyhaus.R;
import com.emptyhaus.adapter.AdapterCallback;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.model.CurrentLocation;
import com.emptyhaus.model.Data;
import com.emptyhaus.model.Place;
import com.emptyhaus.viewModel.EmptyHausViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectLocationActivity extends BaseActivity {
    ListPopupWindow listPopupWindow;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    Activity mActivity = SelectLocationActivity.this;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.rlDropDown)
    RelativeLayout rlDropDown;
    @BindView(R.id.tvPopuPSelected)
    TextView tvPopuPSelected;
    List<Place> list = new ArrayList<>();
    List<String> mlist = new ArrayList<>();
    Data data;
    @BindView(R.id.ivNext)
    ImageView ivNext;


    EmptyHausViewModel emptyHausViewModel;
    AdapterCallback adapterCallback = new AdapterCallback() {
        @Override
        public void onMethodCallback(Boolean t, String tii) {
            if (t) {
                tvPopuPSelected.setText(tii);
                recyclerview.setVisibility(View.INVISIBLE);
            } else {
                recyclerview.setVisibility(View.VISIBLE);
            }
        }
    };
    @BindView(R.id.tvEnterEmail)
    TextViewBold tvEnterEmail;
    @BindView(R.id.pbBar)
    ProgressBar pbBar;
    @BindView(R.id.ivDropDown)
    ImageView ivDropDown;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);

        data = (Data) getIntent().getSerializableExtra(getString(R.string.data));
        emptyHausViewModel = new EmptyHausViewModel(mActivity);
        ButterKnife.bind(this);

        emptyHausViewModel.getLocationObject().observe((LifecycleOwner) mActivity, new Observer<CurrentLocation>() {
            @Override
            public void onChanged(@Nullable CurrentLocation currentLocation) {
                pbBar.setVisibility(View.GONE);
                if (currentLocation.getStatus().equals("1")) {
                    if (currentLocation.getData().size() > 0)
                        list.addAll(currentLocation.getData());

                    for (int j = 0; j < list.size(); j++) {
                        String s = currentLocation.getData().get(j).getLocation();
                        mlist.add(s);
                    }
                    listPopupWindow.show();
                }

            }
        });

        listPopupWindow = new ListPopupWindow(
                mActivity);
        listPopupWindow.setAdapter(new ArrayAdapter(
                mActivity, R.layout.listpopup
                , mlist));
        listPopupWindow.setAnchorView(tvPopuPSelected);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvPopuPSelected.setText(mlist.get(i));
                listPopupWindow.dismiss();
            }
        });


    }

    @OnClick({R.id.ivBack, R.id.rlDropDown, R.id.ivNext, R.id.tvPopuPSelected})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvPopuPSelected:
                if (pbBar.getVisibility() == View.GONE) {
                    if (isNetworkAvailable(mActivity)) {
                        if (mlist.size() == 0) {
                            pbBar.setVisibility(View.VISIBLE);
                            emptyHausViewModel.getAllLocation();
                        } else if (mlist.size() > 0) {
                            listPopupWindow.show();
                        }
                    } else
                        showErrorMessages(mActivity, getString(R.string.internet));
                }
                break;
            case R.id.ivBack:
                finish();
                break;
            case R.id.rlDropDown:
                if (pbBar.getVisibility() == View.GONE) {
                    if (isNetworkAvailable(mActivity)) {
                        if (mlist.size() == 0) {

                            pbBar.setVisibility(View.VISIBLE);
                            emptyHausViewModel.getAllLocation();
                        } else if (mlist.size() > 0) {
                            listPopupWindow.show();
                        }
                    } else
                        showErrorMessages(mActivity, getString(R.string.internet));
                }

                break;
            case R.id.ivNext:

                if (!tvPopuPSelected.getText().equals(getString(R.string.selectLoc))) {
                    data.setLocation(tvPopuPSelected.getText().toString().trim());
                    Intent intent = new Intent(mActivity, CitiesSelectionActivity.class);
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getLocation().equals(data.getLocation())) {
                            data.setCityId(list.get(i).getId());
                        }
                    }

                    intent.putExtra(getString(R.string.data), data);
                    startActivity(intent);
                } else {
                    showToastAlert(mActivity, getString(R.string.selectLocationc));
                }

                break;
        }
    }


}
