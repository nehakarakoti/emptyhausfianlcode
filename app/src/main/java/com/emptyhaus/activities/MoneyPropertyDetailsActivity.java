package com.emptyhaus.activities;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.emptyhaus.R;
import com.emptyhaus.fonts.EditTextRegular;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.model.Data;
import com.emptyhaus.model.SignUp;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.viewModel.EmptyHausViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MoneyPropertyDetailsActivity extends BaseActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    EmptyHausViewModel emptyHausViewModel;
    Data data;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    @BindView(R.id.tvEnterEmail)
    TextViewBold tvEnterEmail;
    @BindView(R.id.etMin)
    EditTextRegular etMin;
    @BindView(R.id.llMini)
    LinearLayout llMini;
    @BindView(R.id.etMax)
    EditTextRegular etMax;
    @BindView(R.id.llMax)
    LinearLayout llMax;
    Activity mActivity = MoneyPropertyDetailsActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_property_details);
        ButterKnife.bind(this);
        if (getIntent() != null)
            data = (Data) getIntent().getSerializableExtra(getString(R.string.data));
        emptyHausViewModel = new EmptyHausViewModel(mActivity);
        emptyHausViewModel.getSignUp().observe(this, new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp signUp) {
                if (signUp != null) {
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.EMAIL, signUp.getData().getEmail());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.ID, signUp.getData().getId());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.USER_NAME, signUp.getData().getUsername());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.NAME, signUp.getData().getName());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FIRST_NAME, signUp.getData().getFirst_name());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FULL_NAME, signUp.getData().getFirst_name() + signUp.getData().getLast_name());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.LAST_NAME, signUp.getData().getLast_name());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.NICKNAME, signUp.getData().getNickname());
                    showToastAlert(mActivity, signUp.getMessage());
                    Intent i = new Intent(mActivity, MainActivity.class);
                    startActivity(i);
                    finishAffinity();

                }
            }
        });
    }

    @OnClick({R.id.ivBack, R.id.ivNext})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivNext:
                nextClick();
                break;
        }
    }

    private void nextClick() {
        if (etMin.getText().toString().trim().equals("")) {
            showErrorMessages(mActivity, getString(R.string.enterminvalue));
        } else if (etMax.getText().toString().trim().equals("")) {
            showErrorMessages(mActivity, getString(R.string.entermaxvalue));
        } else {
            data.setMax_budget(etMax.getText().toString().trim());
            data.setMin_budget(etMin.getText().toString().trim());
            if (isNetworkAvailable(mActivity)) {
                emptyHausViewModel.SignUp(data, mActivity);
            } else
                showErrorMessages(mActivity, getString(R.string.internet));
        }

    }
}
