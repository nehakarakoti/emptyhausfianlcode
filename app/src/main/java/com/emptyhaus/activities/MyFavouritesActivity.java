package com.emptyhaus.activities;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emptyhaus.R;
import com.emptyhaus.adapter.FavouritesAdapter;
import com.emptyhaus.fonts.ButtonMedium;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.fonts.TextViewMedium;
import com.emptyhaus.model.FavList;
import com.emptyhaus.model.HomeObject;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.viewModel.EmptyHausViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyFavouritesActivity extends BaseActivity {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    Activity mActivity = MyFavouritesActivity.this;
    EmptyHausViewModel emptyHausViewModel;
    List<HomeObject> homeObjects = new ArrayList<>();
    FavouritesAdapter fAdapter;
    @BindView(R.id.tvNodata)
    TextView tvNodata;
    @BindView(R.id.tvFav)
    TextViewBold tvFav;
    @BindView(R.id.tv)
    TextViewMedium tv;
    @BindView(R.id.btnCall)
    ButtonMedium btnCall;
    @BindView(R.id.ll)
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favourites);
        ButterKnife.bind(this);
        emptyHausViewModel = new EmptyHausViewModel(mActivity);
        fAdapter = new FavouritesAdapter(mActivity, homeObjects);
        recyclerView.setAdapter(fAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        emptyHausViewModel.getFavListMutableLiveData().observe((LifecycleOwner) mActivity, new Observer<FavList>() {
            @Override
            public void onChanged(@Nullable FavList home) {
                if (home != null) {
                    if (home.getStatus().equals("1")) {

                        homeObjects.addAll(home.getData());
                        if (homeObjects.size() != 0) {
                            tvNodata.setVisibility(View.GONE);
                            fAdapter.notifyDataSetChanged();
                        } else
                            tvNodata.setVisibility(View.VISIBLE);
                    } else {

                        tvNodata.setVisibility(View.VISIBLE);
                    }


                }
            }
        });
        if (isNetworkAvailable(mActivity))
            emptyHausViewModel.favList(EmptyHausPreference.readString(mActivity, EmptyHausPreference.ID, ""));
        else
            showErrorMessages(mActivity, getString(R.string.internet));
    }

    @OnClick(R.id.btnCall)
    public void onClick() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:0123456789"));
        startActivity(intent);
    }
}
