package com.emptyhaus.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.emptyhaus.R;
import com.emptyhaus.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutUsActivity extends BaseActivity {
    Activity mActivity = AboutUsActivity.this;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.help_webview)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        webView = (WebView) findViewById(R.id.help_webview);
        showProgressDialog(AboutUsActivity.this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(Constants.ABOUT_US);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                showProgressDialog(mActivity);
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dismissProgressDailog();
            }

            @OnClick(R.id.ivBack)
            public void onClick() {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
//    public void webViewMethod(WebView webView){
//        webView.setWebViewClient(new WebViewClient() {
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                showProgressDialog(mActivity);
//                view.loadUrl(url);
//                return true;
//            }
//
//            public void onPageFinished(WebView view, String url) {
//                dismissProgressDailog();
//            }
//
//            @OnClick(R.id.ivBack)
//            public void onClick() {
//                onBackPressed();
//            }
//        });
}
