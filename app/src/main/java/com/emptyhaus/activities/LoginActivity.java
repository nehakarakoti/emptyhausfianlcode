package com.emptyhaus.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.emptyhaus.R;
import com.emptyhaus.utils.EmptyHausPreference;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {
    private static final String TAG = "LoginId";
    private static final int RC_SIGN_IN = 234;
    Activity mActivity = LoginActivity.this;
    @BindView(R.id.txtCreateNewACTV)
    TextView txtCreateNewACTV;
    @BindView(R.id.tvLogin)
    TextView tvLogin;
    @BindView(R.id.ivGoogle)
    ImageView ivGoogle;
    @BindView(R.id.rlGoogle)
    RelativeLayout rlGoogle;
    @BindView(R.id.rlFacebook)
    RelativeLayout rlFacebook;
    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;
    @BindView(R.id.login_button)
    LoginButton fb_origin_login_button;
    FirebaseAuth mAuth;
    TextToSpeech textToSpeech;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);


        ButterKnife.bind(this);
        callbackManager = CallbackManager.Factory.create();
        googleLoginConfig();
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.ENGLISH);

                }
            }
        });
    }

    @OnClick({R.id.tvLogin, R.id.txtCreateNewACTV, R.id.ivGoogle, R.id.rlGoogle, R.id.rlFacebook})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tvLogin:
                startActivity(new Intent(mActivity, SignInActivity.class));
                break;
            case R.id.txtCreateNewACTV:
                startActivity(new Intent(mActivity, SignUpActivity.class));
                break;
            case R.id.rlGoogle:

                signIn();
                break;
            case R.id.rlFacebook:
                LoginManager.getInstance().logOut();
                fb_origin_login_button.performClick();


                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        String fbUserID = loginResult.getAccessToken().getUserId();
                                        Log.v("LoginActivity", response.toString());
                                        String imageUrl = "https://graph.facebook.com/" + fbUserID + "/picture?type=large";
                                        // Application code
                                        try {
                                            Log.i("kfjasdklfjakldfj", imageUrl);
                                            String name = object.getString("name");
                                            EmptyHausPreference.writeString(mActivity, EmptyHausPreference.PROFILE_PIC, imageUrl);
                                            EmptyHausPreference.writeString(mActivity, EmptyHausPreference.USER_NAME, name);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            String birthday = object.getString("birthday"); // 01/31/1980 format
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                        startActivity(new Intent(mActivity, MainActivity.class));

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "name");
                        request.setParameters(parameters);
                        request.executeAsync();
//                        startActivity(new Intent(mActivity, MainActivity.class));
                        //getFbProfile(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        //Toast.makeText(ContinueActivity.this, "Cancelled by User", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {

                    }
                });

                break;
        }
    }


    private void googleLoginConfig() {
        FirebaseApp.initializeApp(getApplicationContext());
        mAuth = FirebaseAuth.getInstance();

        //Then we need a GoogleSignInOptions object
        //And we need to build it as below
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        //Then we will get the GoogleSignInClient object from GoogleSignIn class
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RC_SIGN_IN) {

            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);

                //authenticating with firebase
                firebaseAuthWithGoogle(account);
                //   progress_bar.setVisibility(View.VISIBLE);
            } catch (ApiException e) {
                System.out.println("ERROR===" + e);
                // Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {

            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        //getting the auth credential
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        //Now using firebase we are signing in the user here
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            String image = user.getPhotoUrl().toString();
                            if (!image.equals(""))
                                EmptyHausPreference.writeString(mActivity, EmptyHausPreference.PROFILE_PIC, image);


                            EmptyHausPreference.writeString(mActivity, EmptyHausPreference.USER_NAME, task.getResult().getUser().getDisplayName());


                            startActivity(new Intent(mActivity, MainActivity.class));

                        } else {

                            Toast.makeText(getApplicationContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }


                    }
                });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
//    private void getFbProfile(final AccessToken accessToken) {
//        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
//            @Override
//            public void onCompleted(JSONObject object, GraphResponse response) {
//                String strAccessToken = accessToken.getToken();
//
//              String  fbUserID = accessToken.getUserId();
//             String   fbToken = strAccessToken;
//                try {
//
//               String     str_facebookid = object.getString("id");
//                String    str_facebookname = object.getString("name");
//
//                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.USER_NAME, str_facebookname);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//
//            }
//        });
//        startActivity(new Intent(mActivity, MainActivity.class));
//
//    }
}
