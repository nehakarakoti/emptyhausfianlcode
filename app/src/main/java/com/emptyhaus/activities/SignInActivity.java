package com.emptyhaus.activities;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.emptyhaus.R;
import com.emptyhaus.fonts.EditTextRegular;
import com.emptyhaus.model.SignUp;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.viewModel.EmptyHausViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity {
    Activity mActivity = SignInActivity.this;
    @BindView(R.id.etEmail)
    EditTextRegular etEmail;
    @BindView(R.id.etPassword)
    EditTextRegular etPassword;
    @BindView(R.id.ivNext)
    TextView ivNext;
    EmptyHausViewModel emptyHausViewModel;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        emptyHausViewModel = new EmptyHausViewModel(mActivity);

        emptyHausViewModel.getSignUp().observe(this, new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp signUp) {
                if (signUp != null) {
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.ID, signUp.getData().getId());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.USER_NAME, signUp.getData().getFirstname() + signUp.getData().getLastname());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FULL_NAME, signUp.getData().getName());

                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.NAME, signUp.getData().getName());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FIRST_NAME, signUp.getData().getFirstname());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.LAST_NAME, signUp.getData().getLastname());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.EMAIL, etEmail.getText().toString().trim());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FULL_NAME, signUp.getData().getName());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MIN_BUDGET, signUp.getData().getMin_budget());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MAX_BUDGET, signUp.getData().getMax_budget());
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.LOCATION, signUp.getData().getLocation());

                    showToastAlert(mActivity, signUp.getMessage());
                    startActivity(new Intent(mActivity, MainActivity.class));
                    finishAffinity();

                }
            }
        });

    }

    @OnClick({R.id.ivBack, R.id.ivNext})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivNext:
                if (etEmail.getText().toString().trim().equals("")) {
            showErrorMessages(mActivity, getString(R.string.enterEmail));
        } else if (!etEmail.getText().toString().trim().contains(getString(R.string.emailcheck))) {
            showErrorMessages(mActivity, getString(R.string.entervalidEmail));
        } else if (!etEmail.getText().toString().trim().contains(getString(R.string.emailcomcheck))) {
            showErrorMessages(mActivity, getString(R.string.entervalidEmail));
        } else if (etPassword.getText().toString().trim().equals("")) {
            showErrorMessages(mActivity, getString(R.string.enterPassword));

        } else {
            if (isNetworkAvailable(mActivity))
                emptyHausViewModel.signIn(mActivity, etEmail.getText().toString().trim(), etPassword.getText().toString().trim());
            else
                showErrorMessages(mActivity, getString(R.string.internet));

        }

                break;
        }
    }

//    @OnClick(R.id.ivNext)
//    public void onClick() {
//
//        if (etEmail.getText().toString().trim().equals("")) {
//            showErrorMessages(mActivity, getString(R.string.enterEmail));
//        } else if (!etEmail.getText().toString().trim().contains(getString(R.string.emailcheck))) {
//            showErrorMessages(mActivity, getString(R.string.entervalidEmail));
//        } else if (!etEmail.getText().toString().trim().contains(getString(R.string.emailcomcheck))) {
//            showErrorMessages(mActivity, getString(R.string.entervalidEmail));
//        } else if (etPassword.getText().toString().trim().equals("")) {
//            showErrorMessages(mActivity, getString(R.string.enterPassword));
//
//        } else {
//            if (isNetworkAvailable(mActivity))
//                emptyHausViewModel.signIn(mActivity, etEmail.getText().toString().trim(), etPassword.getText().toString().trim());
//            else
//                showErrorMessages(mActivity, getString(R.string.internet));
//
//        }
//
//    }

//    public void signIn() {
//        showProgressDialog(mActivity);
//        RestClient.get().logIn(etEmail.getText().toString().trim(), etPassword.getText().toString().trim()).enqueue(new Callback<SignUp>() {
//            @Override
//            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
//                dismissProgressDailog();
//                if (response.isSuccessful() && response.body() != null) {
//                    if (response.body().getStatus().equals("1")) {
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.ID, response.body().getData().getId());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.USER_NAME, response.body().getData().getFirstname()+response.body().getData().getLastname());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.NAME, response.body().getData().getName());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FIRST_NAME, response.body().getData().getFirstname());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.LAST_NAME, response.body().getData().getLastname());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.EMAIL, etEmail.getText().toString().trim());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FULL_NAME, response.body().getData().getName());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MIN_BUDGET, response.body().getData().getMin_budget());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MAX_BUDGET, response.body().getData().getMax_budget());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.LOCATION, response.body().getData().getLocation());
//
//                        showToastAlert(mActivity,response.body().getMessage());
//                        startActivity(new Intent(mActivity, MainActivity.class));
//                        finishAffinity();
//
//                    }
//                    else {
//                        showErrorMessages(mActivity,response.body().getMessage());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SignUp> call, Throwable t) {
//
//            }
//        });
//    }
}
