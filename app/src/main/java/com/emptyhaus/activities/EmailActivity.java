package com.emptyhaus.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.emptyhaus.R;
import com.emptyhaus.fonts.EditTextRegular;
import com.emptyhaus.model.Data;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmailActivity extends BaseActivity {
    Activity mActivity = EmailActivity.this;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.etEmail)
    EditTextRegular etEmail;
    Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        ButterKnife.bind(this);
        data = (Data) getIntent().getSerializableExtra(getString(R.string.data));

    }

    @OnClick({R.id.ivBack, R.id.ivNext})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivNext:
                if (etEmail.getText().toString().trim().equals("")) {
                    showErrorMessages(mActivity, getString(R.string.enterEmail));
                } else if (!etEmail.getText().toString().trim().contains(getString(R.string.emailcheck))) {
                    showErrorMessages(mActivity, getString(R.string.entervalidEmail));
                } else if (!etEmail.getText().toString().trim().contains(getString(R.string.emailcomcheck))) {
                    showErrorMessages(mActivity, getString(R.string.entervalidEmail));
                } else {
                    if (data != null) {
                        data.setEmail(etEmail.getText().toString().trim());
                        Intent intent = new Intent(mActivity, EnterPasswordActivity.class);
                        intent.putExtra(getString(R.string.data), data);
                        startActivity(intent);
                    }
                }
                break;
        }
    }


}
