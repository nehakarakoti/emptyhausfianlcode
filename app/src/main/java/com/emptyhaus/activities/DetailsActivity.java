package com.emptyhaus.activities;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.emptyhaus.R;
import com.emptyhaus.adapter.SlidingImage_Adapter;
import com.emptyhaus.dialog.ProjectDescription;
import com.emptyhaus.dialog.RatingDaialog;
import com.emptyhaus.fonts.ButtonRegular;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.fonts.TextViewMedium;
import com.emptyhaus.fonts.TextViewRegular;
import com.emptyhaus.model.Content;
import com.emptyhaus.model.Favourite;
import com.emptyhaus.model.HomeObject;
import com.emptyhaus.model.ItemDetails;
import com.emptyhaus.model.Title;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.viewModel.EmptyHausViewModel;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends BaseActivity {
    Activity mActivity = DetailsActivity.this;
    ArrayList<String> images = new ArrayList<>();
    //private static final Integer[] IMAGES = {R.drawable.img_details, R.drawable.img_details, R.drawable.img_details, R.drawable.img_details, R.drawable.img_details};
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    @BindView(R.id.ivRight)
    ImageView ivRight;
    @BindView(R.id.ivLeft)
    ImageView ivLeft;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.rlUsa)
    RelativeLayout rlUsa;
    @BindView(R.id.ivProfile)
    ImageView ivProfile;
    @BindView(R.id.role)
    TextViewRegular role;
    @BindView(R.id.tvName)
    TextViewRegular tvName;
    @BindView(R.id.tvRate)
    TextViewRegular tvRate;
    @BindView(R.id.tvRequest)
    TextViewRegular tvRequest;
    @BindView(R.id.btnCall)
    ButtonRegular btnCall;
    @BindView(R.id.tvTittle)
    TextViewBold tvTittle;
    @BindView(R.id.tvLocality)
    TextViewRegular tvLocality;
    @BindView(R.id.tvArea)
    TextViewRegular tvArea;
    @BindView(R.id.tvBedroom)
    TextViewRegular tvBedroom;
    @BindView(R.id.tvBathroom)
    TextViewRegular tvBathroom;
    @BindView(R.id.tvpropertyDescription)
    TextViewRegular tvpropertyDescription;
    @BindView(R.id.btnSeeMore)
    Button btnSeeMore;
    @BindView(R.id.tvPrice)
    TextViewMedium tvPrice;
    @BindView(R.id.ivheartActive)
    ImageView ivheartActive;
    @BindView(R.id.pbfavourite)
    ProgressBar pbfavourite;
    EmptyHausViewModel emptyHausViewModel;
    String fav_status = "";
    @BindView(R.id.ivShare)
    ImageView ivShare;
    private ArrayList<String> ImagesArray = new ArrayList<String>();
    Title title;
    Content content;
String id="";
    HomeObject homeObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        emptyHausViewModel = new EmptyHausViewModel(mActivity);
        if (getIntent().getParcelableExtra(getString(R.string.comingId)) != null) {
            homeObject = getIntent().getParcelableExtra(getString(R.string.comingId));
            id=homeObject.getId();
            fav_status=getIntent().getStringExtra(getString(R.string.favStatus));
            //emptyHausViewModel.ItemDetails(id,EmptyHausPreference.readString(mActivity,EmptyHausPreference.ID,""));
if(getIntent().getParcelableExtra(getString(R.string.titleContent))!=null){
    content=getIntent().getParcelableExtra(getString(R.string.titleContent));
}
            if(getIntent().getParcelableExtra(getString(R.string.tittleObject))!=null){
title=getIntent().getParcelableExtra(getString(R.string.tittleObject));
            }
            images.add(homeObject.getImage());
            ivLeft.setVisibility(View.GONE);
            ivRight.setVisibility(View.GONE);
            init();

            tvLocality.setText(homeObject.getAddress());
            tvPrice.setText("$"+homeObject.getPrice().toString());
            //tvRate.setText(homeObject.getPrice());
            tvArea.setText(homeObject.getArea() + " " + getResources().getString(R.string.sq));
            tvBathroom.setText(homeObject.getBathroom() + " " + getResources().getString(R.string.bathroom));
            tvBedroom.setText(homeObject.getBedroom() + " " + getResources().getString(R.string.bedroom));


            tvTittle.setText(title.getRendered());
            fav_status=homeObject.getFavorite_status();

            if (content.getRendered().toString().trim().length() > 240) {
                btnSeeMore.setVisibility(View.VISIBLE);
            } else {
                btnSeeMore.setVisibility(View.GONE);
            }
            if (fav_status.equals("0")) {
                ivheartActive.setImageDrawable(getDrawable(R.drawable.heart2));
            } else if (fav_status.equals("1")) {
                ivheartActive.setImageDrawable(getDrawable(R.drawable.heart));
            }
            String plainText = Html.fromHtml(content.getRendered().toString().trim()).toString();
            tvpropertyDescription.setText(plainText);
            emptyHausViewModel.ItemDetails(id,EmptyHausPreference.readString(mActivity,EmptyHausPreference.ID,""));

        }
        emptyHausViewModel.getItemDetailsMutableLiveData().observe((LifecycleOwner) mActivity, new Observer<ItemDetails>() {
            @Override
            public void onChanged(@Nullable ItemDetails itemDetails) {
                if(itemDetails!=null&&itemDetails.getStatus().equals("1")){
                    homeObject=itemDetails.getData();
            images.add(homeObject.getImage());
            ivLeft.setVisibility(View.GONE);
            ivRight.setVisibility(View.GONE);
                    init();

            tvLocality.setText(homeObject.getAddress());
            tvPrice.setText("$"+homeObject.getPrice().toString());
            //tvRate.setText(homeObject.getPrice());
            tvArea.setText(homeObject.getArea() + " " + getResources().getString(R.string.sq));
            tvBathroom.setText(homeObject.getBathroom() + " " + getResources().getString(R.string.bathroom));
            tvBedroom.setText(homeObject.getBedroom() + " " + getResources().getString(R.string.bedroom));
            title=homeObject.getTitle();

                tvTittle.setText(title.getRendered());
fav_status=homeObject.getFavorite_status();

                content = homeObject.getContent();
                if (content.getRendered().toString().trim().length() > 240) {
                    btnSeeMore.setVisibility(View.VISIBLE);
                } else {
                    btnSeeMore.setVisibility(View.GONE);
                }
                if (fav_status.equals("0")) {
                    ivheartActive.setImageDrawable(getDrawable(R.drawable.heart2));
                } else if (fav_status.equals("1")) {
                    ivheartActive.setImageDrawable(getDrawable(R.drawable.heart));
                }
                String plainText = Html.fromHtml(content.getRendered().toString().trim()).toString();
                tvpropertyDescription.setText(plainText);
            }



                }

        });
        emptyHausViewModel.getFavouriteMutableLiveData().observe((LifecycleOwner) mActivity, new Observer<Favourite>() {
            @Override
            public void onChanged(@Nullable Favourite favourite) {

                pbfavourite.setVisibility(View.GONE);

            }
        });

    }

    private void init() {

        for (int i = 0; i < images.size(); i++)
            ImagesArray.add(images.get(i));

        mPager = (ViewPager) findViewById(R.id.viewPager);


        mPager.setAdapter(new SlidingImage_Adapter(mActivity, images));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);
        if (images.size() < 2) {
            indicator.setVisibility(View.GONE);
        }
        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = images.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
//        Timer swipeTimer = new Timer();
//        swipeTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {


                if (i == viewPager.getAdapter().getCount() - 1) {
                    ivLeft.setVisibility(View.VISIBLE);
                    ivRight.setVisibility(View.GONE);
                } else if (i == 0) {
                    ivRight.setVisibility(View.VISIBLE);
                    ivLeft.setVisibility(View.GONE);
                } else {
                    ivLeft.setVisibility(View.VISIBLE);
                    ivRight.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

    }


//    }

    private void rightClick() {
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
    }

    private void leftClick() {
        if (viewPager.getCurrentItem() != 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);

        }
    }

    @OnClick({R.id.tvRate,R.id.ivProfile,R.id.ivShare, R.id.tvRequest, R.id.ivLeft, R.id.ivRight, R.id.btnCall, R.id.btnSeeMore, R.id.ivheartActive})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivProfile:
                startActivity(new Intent(mActivity,CallingDetailsActivity.class));
                break;
            case R.id.tvRate:
                new RatingDaialog(DetailsActivity.this).showDialog();
                break;
            case R.id.tvRequest:
                startActivity(new Intent(mActivity,MessageActivity.class));

                break;
            case R.id.ivLeft:
                leftClick();
                break;
            case R.id.ivRight:
                rightClick();
                break;
            case R.id.btnCall:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0123456789"));
                startActivity(intent);
                break;
            case R.id.btnSeeMore:
                new ProjectDescription(mActivity).showDialog(content.getRendered().toString().trim());

                break;
            case R.id.ivShare:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                String str3 =title.getRendered() + '\n' + content.getRendered();
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(Html.fromHtml(str3).toString()));
                startActivity(shareIntent);
                break;
            case R.id.ivheartActive:
                if (fav_status.equals("0")) {
                    ivheartActive.setImageDrawable(getDrawable(R.drawable.heart));
                    pbfavourite.setVisibility(View.VISIBLE);
                    if (isNetworkAvailable(mActivity))
                        fav_status = "1";
                    emptyHausViewModel.addingToFavourite(EmptyHausPreference.readString(mActivity, EmptyHausPreference.ID, ""), homeObject.getId());

                    break;
                }
        }
    }


}
