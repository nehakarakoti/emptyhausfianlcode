package com.emptyhaus.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.emptyhaus.R;
import com.emptyhaus.utils.Constants;
import com.kaopiz.kprogresshud.KProgressHUD;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactUsActivity extends Activity {
    public KProgressHUD kProgressHUD;
    Activity mActivity = ContactUsActivity.this;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.help_webview)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        kProgressHUD = KProgressHUD.create(mActivity).setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAnimationSpeed(Constants.PROGRESS)
                .setDimAmount(Constants.PROGRESSAMOUNT).show();
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(Constants.CONTACT_US);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                kProgressHUD.show();
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                kProgressHUD.dismiss();
            }

//            @OnClick(R.id.ivBack)
//            public void onClick() {
//                finish();
//            }
        });
    }

    public void showProgressDialog() {
        kProgressHUD = KProgressHUD.create(mActivity).setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAnimationSpeed(Constants.PROGRESS)
                .setDimAmount(Constants.PROGRESSAMOUNT)
                .show();
    }

    //Hide Progress Dialog
    public void dismissProgressDailog() {
        kProgressHUD.dismiss();
    }
}
