package com.emptyhaus.activities;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emptyhaus.R;
import com.emptyhaus.fonts.EditTextRegular;
import com.emptyhaus.fonts.TextViewMedium;
import com.emptyhaus.model.SignUp;
import com.emptyhaus.utils.Constants;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.viewModel.EmptyHausViewModel;
import com.emptyhaus.webservices.RestClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {
    public static String fullName = "", minVal = "", maxVal = "";
    ;
    Activity mActivity = EditProfileActivity.this;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.rlToolbar)
    RelativeLayout rlToolbar;
    @BindView(R.id.etFullName)
    EditText etFullName;
    @BindView(R.id.edit1)
    ImageView edit1;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.edit2)
    ImageView edit2;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.edit3)
    ImageView edit3;
    @BindView(R.id.ivPhotoUpload)
    ImageView ivPhotoUpload;
    @BindView(R.id.et2)
    EditTextRegular et2;
    @BindView(R.id.et3)
    EditTextRegular et3;
    String mImagePath;
    @BindView(R.id.ivProfilePhoto)
    ImageView ivProfilePhoto;
    @BindView(R.id.tvChangeLocation)
    TextViewMedium tvChangeLocation;
    @BindView(R.id.pbEdit1)
    ProgressBar pbEdit1;
    @BindView(R.id.pbEdit2)
    ProgressBar pbEdit2;
    @BindView(R.id.pbEditmin)
    ProgressBar pbEditmin;
    @BindView(R.id.pbEditmax)
    ProgressBar pbEditmax;
    @BindView(R.id.editmin)
    ImageView editmin;
    @BindView(R.id.editmax)
    ImageView editmax;
    @BindView(R.id.tvName)
    TextView tvName;
    EmptyHausViewModel emptyHausViewModel;
    private Bitmap bMapRotate;
    private int mOrientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        emptyHausViewModel = new EmptyHausViewModel(mActivity);
        emptyHausViewModel.getSignUp().observe(EditProfileActivity.this, new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp signUp) {
                EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FULL_NAME, signUp.getData().getName());
                EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MIN_BUDGET, signUp.getData().getMeta().getMin_budget());
                EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MAX_BUDGET, signUp.getData().getMeta().getMax_budget());
                EmptyHausPreference.writeString(mActivity, EmptyHausPreference.LOCATION, signUp.getData().getMeta().getLocation());
                String name = EmptyHausPreference.readString(mActivity, EmptyHausPreference.FULL_NAME, "");
                String email = EmptyHausPreference.readString(mActivity, EmptyHausPreference.EMAIL, "");
                String min = EmptyHausPreference.readString(mActivity, EmptyHausPreference.MIN_BUDGET, "");
                String max = EmptyHausPreference.readString(mActivity, EmptyHausPreference.MAX_BUDGET, "");
                if (!name.equals("")) {
                    tvName.setText(name);
                    etFullName.setText(name);
                }
                etEmail.setText(email);
                et2.setText(min);
                et3.setText(max);
                String profilePicture = EmptyHausPreference.readString(mActivity, EmptyHausPreference.PROFILE_PIC, "");
                if (!profilePicture.equals(""))
                    Glide.with(mActivity).load(profilePicture).apply(new RequestOptions().circleCrop()).into(ivProfilePhoto);

            }
        });
        fullName = tvName.getText().toString().trim();
        if (isNetworkAvailable(mActivity))
            emptyHausViewModel.getEditProfileApi(mActivity);
        else {
            String name = EmptyHausPreference.readString(mActivity, EmptyHausPreference.FULL_NAME, "");
            String email = EmptyHausPreference.readString(mActivity, EmptyHausPreference.EMAIL, "");
            String min = EmptyHausPreference.readString(mActivity, EmptyHausPreference.MIN_BUDGET, "");
            String max = EmptyHausPreference.readString(mActivity, EmptyHausPreference.MAX_BUDGET, "");
            if (!name.equals("")) {
                tvName.setText(name);
                etFullName.setText(name);
            }
            etEmail.setText(email);
            et2.setText(min);
            et3.setText(max);
        }

        String profilePicture = EmptyHausPreference.readString(mActivity, EmptyHausPreference.PROFILE_PIC, "");
        if (!profilePicture.equals(""))
            Glide.with(mActivity).load(profilePicture).apply(new RequestOptions().circleCrop()).into(ivProfilePhoto);

    }

    @OnClick({R.id.ivBack, R.id.edit1, R.id.edit2, R.id.edit3, R.id.ivPhotoUpload, R.id.tvChangeLocation, R.id.editmin, R.id.editmax})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.edit1:
                if (!etFullName.getText().equals("")) {

                    etFullName.setSelection(etFullName.length());
                }
                etFullName.requestFocus();
                if (etFullName.getText().toString().trim().equals(""))
                    edit1.setImageDrawable(getResources().getDrawable(R.drawable.edit));
                else {
                    edit1.setImageDrawable(getResources().getDrawable(R.drawable.check));
                    if (!etFullName.getText().toString().trim().equals(EmptyHausPreference.readString(mActivity, EmptyHausPreference.FULL_NAME, ""))) {
                        edit1.setVisibility(View.GONE);
                        pbEdit1.setVisibility(View.VISIBLE);
                        if (isNetworkAvailable(mActivity))
                            userNameApi();
                        else {
                            edit1.setVisibility(View.VISIBLE);
                            pbEdit1.setVisibility(View.GONE);

                            showToastAlert(mActivity, getString(R.string.internet));
                        }
                    }
                }

                break;
            case R.id.edit2:
                if (etEmail.getText().toString().trim().equals(""))
                    edit2.setImageDrawable(getResources().getDrawable(R.drawable.edit));
                else
                    edit2.setImageDrawable(getResources().getDrawable(R.drawable.check));
                break;
            case R.id.edit3:
                if (!etPassword.getText().equals("")) {

                    etPassword.setSelection(etPassword.length());

                }
                etPassword.requestFocus();
                if (etPassword.getText().toString().trim().equals(""))
                    edit3.setImageDrawable(getResources().getDrawable(R.drawable.edit));
                else
                    edit3.setImageDrawable(getResources().getDrawable(R.drawable.check));
                if (!etPassword.getText().toString().trim().equals(EmptyHausPreference.readString(mActivity, EmptyHausPreference.PASSWORD, ""))) {
                    edit3.setVisibility(View.GONE);
                    pbEdit2.setVisibility(View.VISIBLE);
                    if (isNetworkAvailable(mActivity))
                        passwordUpdate();
                    else {
                        edit3.setVisibility(View.VISIBLE);
                        pbEdit2.setVisibility(View.GONE);

                        showToastAlert(mActivity, getString(R.string.internet));
                    }
                }
                break;
            case R.id.editmin:
                if (!et2.getText().equals("")) {
                    et2.requestFocus();
                    et2.setSelection(et2.length());

                }
                et2.requestFocus();
                if (et2.getText().toString().trim().equals(""))
                    editmin.setImageDrawable(getResources().getDrawable(R.drawable.edit));
                else
                    editmin.setImageDrawable(getResources().getDrawable(R.drawable.check));
                if (!et2.getText().toString().trim().equals(EmptyHausPreference.readString(mActivity, EmptyHausPreference.MIN_BUDGET, ""))) {
                    editmin.setVisibility(View.GONE);
                    pbEditmin.setVisibility(View.VISIBLE);
                    if (isNetworkAvailable(mActivity))
                        updateMin();

                    else {
                        editmin.setVisibility(View.VISIBLE);
                        pbEditmin.setVisibility(View.GONE);
                        showToastAlert(mActivity, getString(R.string.internet));
                    }
                }


                break;
            case R.id.editmax:
                if (!et3.getText().equals("")) {
                    et3.requestFocus();
                    et3.setSelection(et3.length());

                }
                et3.requestFocus();
                if (et3.getText().toString().trim().equals(""))
                    editmax.setImageDrawable(getResources().getDrawable(R.drawable.edit));
                else
                    editmax.setImageDrawable(getResources().getDrawable(R.drawable.check));
                if (!et3.getText().toString().trim().equals(EmptyHausPreference.readString(mActivity, EmptyHausPreference.MAX_BUDGET, ""))) {
                    editmax.setVisibility(View.GONE);
                    pbEditmax.setVisibility(View.VISIBLE);
                    if (isNetworkAvailable(mActivity))
                        updateMax();

                    else {
                        editmax.setVisibility(View.VISIBLE);
                        pbEditmax.setVisibility(View.GONE);
                        showToastAlert(mActivity, getString(R.string.internet));
                    }
                }

                break;
            case R.id.ivPhotoUpload:
                checkPermissions();
                break;
            case R.id.tvChangeLocation:
                startActivity(new Intent(mActivity, UpdateLocationActivity.class));

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {


        }

        if (requestCode == Constants.PICK_IMAGE_CAMERA && resultCode == Activity.RESULT_OK) {
            File imageFile = saveImageToExternalStorage
                    (getFile(mImagePath, EditProfileActivity.this, mOrientation, bMapRotate), EditProfileActivity.this);
            Uri imageObtained = Uri.fromFile(imageFile);

            Glide.with(mActivity)
                    .load(imageObtained)
                    .apply(RequestOptions.circleCropTransform())
                    .into(ivProfilePhoto);
            try {

                InputStream inputStream = getContentResolver().openInputStream(imageObtained);
                Bitmap image = BitmapFactory.decodeStream(inputStream);
                Glide.with(mActivity)
                        .load(image)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivProfilePhoto);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, getString(R.string.unable), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == Constants.PICK_IMAGE_MULTIPLE && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            File imageFile = saveImageToExternalStorage
                    (getFile(picturePath, EditProfileActivity.this, mOrientation, bMapRotate), EditProfileActivity.this);
            Uri imageObtained = Uri.fromFile(imageFile);

            try {

                InputStream inputStream = getContentResolver().openInputStream(imageObtained);

                Bitmap image = BitmapFactory.decodeStream(inputStream);
                Glide.with(mActivity)
                        .load(image)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivProfilePhoto);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, getString(R.string.unable), Toast.LENGTH_LONG).show();
            }
        }


    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.
                        WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
                return;
            } else showdialog();
        } else
            showdialog();
    }

    private void showdialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1);
        arrayAdapter.add(getResources().getString(R.string.Gallery));
        arrayAdapter.add(getResources().getString(R.string.Camera));
        arrayAdapter.add(getResources().getString(R.string.Cancel));


        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                if (strName.equals(getResources().getString(R.string.Gallery))) {
                    isStoragePermissionGranted();

                } else if (strName.equals(getResources().getString(R.string.Camera))) {
                    isCameraGranted();
                } else {
                    dialog.dismiss();
                }
            }
        });
        builderSingle.show();
    }


    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, Constants.PICK_IMAGE_MULTIPLE);
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(intent, 1);
            return true;

        }
    }


    public boolean isCameraGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                File f = null;
                try {
                    f = setUpPhotoFile(EditProfileActivity.this);
                    mImagePath = f.getAbsolutePath();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        Uri contentUri = FileProvider.getUriForFile(EditProfileActivity.this,
                                getPackageName(), f);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                contentUri);
                    } else
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(f));
                } catch (IOException e) {
                    e.printStackTrace();
                    f = null;
                    mImagePath = null;
                }
                startActivityForResult(takePictureIntent, Constants.PICK_IMAGE_CAMERA);


                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 2);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation

            Intent takePictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            File f = null;
            try {
                f = setUpPhotoFile(EditProfileActivity.this);
                mImagePath = f.getAbsolutePath();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    Uri contentUri = FileProvider.getUriForFile(EditProfileActivity.this,
                            getPackageName(), f);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            contentUri);
                } else
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(f));
            } catch (IOException e) {
                e.printStackTrace();
                f = null;
                mImagePath = null;
            }
            startActivityForResult(takePictureIntent, Constants.PICK_IMAGE_CAMERA);
            return true;

        }

    }


    public void userNameApi() {
        RestClient.get().updatingUserName(EmptyHausPreference.readString(mActivity, EmptyHausPreference.ID, ""), etFullName.getText().toString().trim()).enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                pbEdit1.setVisibility(View.GONE);
                edit1.setVisibility(View.VISIBLE);
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals("1")) {
                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FULL_NAME, etFullName.getText().toString().trim());
                        tvName.setText(etFullName.getText().toString().trim());
                        fullName = tvName.getText().toString().trim();

                    } else {
                        showToastAlert(mActivity, response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {

            }
        });

    }

    public void passwordUpdate() {
        RestClient.get().updatingPassword(EmptyHausPreference.readString(mActivity, EmptyHausPreference.ID, ""), etPassword.getText().toString().trim()).enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                pbEdit2.setVisibility(View.GONE);
                edit3.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("1")) {
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.PASSWORD, etPassword.getText().toString().trim());


                } else {
                    showToastAlert(mActivity, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {

            }
        });
    }

    public void updateMax() {
        boolean conatinDollar = false;
        for (int i = 0; i < et3.getText().toString().length(); i++) {
            if (et3.getText().toString().charAt(i) == '$') {
                conatinDollar = true;
            }
        }
        if (conatinDollar) {
            maxVal = et3.getText().toString().trim();
        } else {
            maxVal = "$" + et3.getText().toString().trim();
        }
        RestClient.get().maxBudget(EmptyHausPreference.readString(mActivity, EmptyHausPreference.ID, "")
                , maxVal).enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                pbEditmax.setVisibility(View.GONE);

                editmax.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("1")) {
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MAX_BUDGET, maxVal);
                    et3.setText(EmptyHausPreference.readString(mActivity, EmptyHausPreference.MAX_BUDGET, ""));


                } else {
                    showToastAlert(mActivity, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {

            }
        });
    }

    public void updateMin() {

        boolean conatinDollar = false;
        for (int i = 0; i < et2.getText().toString().length(); i++) {
            if (et2.getText().toString().charAt(i) == '$') {
                conatinDollar = true;
            }
        }
        if (conatinDollar) {
            minVal = et2.getText().toString().trim();
        } else {
            minVal = "$" + et2.getText().toString().trim();
        }
        RestClient.get().updateMin(EmptyHausPreference.readString(mActivity, EmptyHausPreference.ID, ""), minVal).enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                pbEditmin.setVisibility(View.GONE);

                editmin.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("1")) {
                    EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MIN_BUDGET, minVal);
                    et2.setText(EmptyHausPreference.readString(mActivity, EmptyHausPreference.MIN_BUDGET, ""));


                } else {
                    showToastAlert(mActivity, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!etFullName.getText().equals("")) {
            etFullName.requestFocus();
            etFullName.setSelection(etFullName.length());
        }
        if (!etPassword.getText().equals("")) {
            etPassword.requestFocus();
            etPassword.setSelection(etPassword.length());

        }
        if (!et2.getText().equals("")) {
            et2.requestFocus();
            et2.setSelection(et2.length());

        }
        if (!et3.getText().equals("")) {
            et3.requestFocus();
            et3.setSelection(et3.length());

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            showdialog();
        }
    }
}
