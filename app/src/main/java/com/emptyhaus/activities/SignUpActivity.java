package com.emptyhaus.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.emptyhaus.R;
import com.emptyhaus.fonts.EditTextRegular;
import com.emptyhaus.model.Data;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity {
    Activity mActivity = SignUpActivity.this;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.etFirstName)
    EditTextRegular etFirstName;
    @BindView(R.id.etLastName)
    EditTextRegular etLastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ivBack, R.id.ivNext})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivNext:
                if (etFirstName.getText().toString().equals("")) {
                    showErrorMessages(mActivity, getString(R.string.enterFirstName));
                } else if (etLastName.getText().toString().equals("")) {
                    showErrorMessages(mActivity, getString(R.string.enterLastName));
                } else {
                    Intent intent = new Intent(mActivity, EmailActivity.class);
                    Data data = new Data();
                    data.setFirst_name(etFirstName.getText().toString().trim());
                    data.setLast_name(etLastName.getText().toString().trim());
                    intent.putExtra(getString(R.string.data), data);
                    startActivity(intent);
                }
                break;
        }
    }

}
