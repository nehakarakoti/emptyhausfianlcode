package com.emptyhaus.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.emptyhaus.R;
import com.emptyhaus.dialog.RatingDaialog;
import com.emptyhaus.fonts.ButtonMedium;
import com.emptyhaus.fonts.TextViewRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CallingDetailsActivity extends AppCompatActivity {
    Activity mActivity = CallingDetailsActivity.this;

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvRate)
    TextView tvRate;
    @BindView(R.id.tvRequest)
    TextView tvRequest;
    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.tvBedRooms)
    TextView tvBedRooms;
    @BindView(R.id.ivBed)
    ImageView ivBed;
    @BindView(R.id.tvBathrroom)
    TextView tvBathrroom;
    @BindView(R.id.ivvBath)
    ImageView ivvBath;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.btnCall)
    ButtonMedium btnCall;
    @BindView(R.id.tvRatings)
    TextView tvRatings;
    @BindView(R.id.tvRent)
    TextView tvRent;
    @BindView(R.id.tvSale)
    TextView tvSale;
    @BindView(R.id.ivImage1)
    ImageView ivImage1;
    @BindView(R.id.tvBedRoom1s)
    TextViewRegular tvBedRoom1s;
    @BindView(R.id.ivBed1)
    ImageView ivBed1;
    @BindView(R.id.tvBathrroom1)
    TextViewRegular tvBathrroom1;
    @BindView(R.id.ivvBath1)
    ImageView ivvBath1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calling_detail_activity);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ivBack, R.id.btnCall, R.id.tvRatings,R.id.tvSale, R.id.tvRent})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvRatings:
                new RatingDaialog(mActivity).showDialog();
                break;
            case R.id.btnCall:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0123456789"));
                startActivity(intent);
                break;
            case R.id.tvSale:

                tvRent.setBackground(mActivity.getDrawable(R.drawable.lightgrety));
                tvSale.setBackground(mActivity.getDrawable(R.drawable.greenbac));
                tvSale.setTextColor(getResources().getColor(R.color.colorWhite));
                tvRent.setTextColor(getResources().getColor(R.color.colorGreen));
                break;
            case R.id.tvRent:
                tvSale.setBackground(mActivity.getDrawable(R.drawable.lightgrety));
                tvRent.setBackground(mActivity.getDrawable(R.drawable.greenbac));
                tvRent.setTextColor(getResources().getColor(R.color.colorWhite));
                tvSale.setTextColor(getResources().getColor(R.color.colorGreen));
                break;
        }
    }


}
