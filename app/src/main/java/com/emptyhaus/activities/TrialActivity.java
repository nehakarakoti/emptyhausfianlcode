package com.emptyhaus.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.emptyhaus.R;

public class TrialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trial);
    }
}
