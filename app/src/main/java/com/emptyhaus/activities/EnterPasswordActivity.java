package com.emptyhaus.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.emptyhaus.R;
import com.emptyhaus.fonts.EditTextRegular;
import com.emptyhaus.model.Data;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterPasswordActivity extends BaseActivity {
    Activity mActivity = EnterPasswordActivity.this;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivNext)
    ImageView ivNext;
    @BindView(R.id.etPassword)
    EditTextRegular etPassword;
    Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_password);
        ButterKnife.bind(this);
        data = (Data) getIntent().getSerializableExtra(getString(R.string.data));
    }

    @OnClick({R.id.ivBack, R.id.ivNext})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivNext:
                if (etPassword.getText().toString().equals(""))
                    showErrorMessages(mActivity, getString(R.string.enterPassword));
                else {
                    data.setPassword(etPassword.getText().toString().trim());
                    Intent i = new Intent(mActivity, SelectLocationActivity.class);
                    i.putExtra(getString(R.string.data), data);
                    startActivity(i);
                }
                break;
        }
    }
}
