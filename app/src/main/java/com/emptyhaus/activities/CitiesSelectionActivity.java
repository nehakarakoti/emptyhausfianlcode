package com.emptyhaus.activities;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.model.CurrentLocation;
import com.emptyhaus.model.Data;
import com.emptyhaus.model.Place;
import com.emptyhaus.viewModel.EmptyHausViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CitiesSelectionActivity extends BaseActivity {
    ListPopupWindow listPopupWindow, listPopupWindow2;
    Activity mActivity = CitiesSelectionActivity.this;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvEnterEmail)
    TextViewBold tvEnterEmail;
    @BindView(R.id.tvPopuPSelected)
    TextViewBold tvPopuPSelected;
    @BindView(R.id.ivDropDown)
    ImageView ivDropDown;
    @BindView(R.id.rlDropDown)
    RelativeLayout rlDropDown;
    @BindView(R.id.tvPopuPSelected2)
    TextViewBold tvPopuPSelected2;
    @BindView(R.id.ivDropDown2)
    ImageView ivDropDown2;
    @BindView(R.id.rlDropDown2)
    RelativeLayout rlDropDown2;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    String id = "";
    Data data;
    List<Place> list1 = new ArrayList<>();
    List<Place> list2 = new ArrayList<>();
    List<String> city1 = new ArrayList<>();
    List<String> city2 = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    @BindView(R.id.pbCity1)
    ProgressBar pbCity1;
    @BindView(R.id.pbCity2)
    ProgressBar pbCity2;
    EmptyHausViewModel emptyHausViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities_selection);
        ButterKnife.bind(this);
        data = (Data) getIntent().getSerializableExtra(getString(R.string.data));
        emptyHausViewModel = new EmptyHausViewModel(mActivity);


        emptyHausViewModel.getLocationObject().observe((LifecycleOwner) mActivity, new Observer<CurrentLocation>() {
            @Override
            public void onChanged(@Nullable CurrentLocation currentLocation) {
                if (currentLocation.getStatus().equals("1")) {
                    pbCity1.setVisibility(View.GONE);
                    if (currentLocation.getData().size() > 0) {
                        list1.clear();
                        list1.addAll(currentLocation.getData());
                        for (int i = 0; i < list1.size(); i++) {
                            String s = list1.get(i).getLocation();
                            city1.add(s);
                        }

                        listPopupWindow.show();
                    }
                } else {


                    showToastAlert(mActivity, currentLocation.getMessage());
                }
            }
        });
        emptyHausViewModel.getSublaoctions().observe((LifecycleOwner) mActivity, new Observer<CurrentLocation>() {
            @Override
            public void onChanged(@Nullable CurrentLocation currentLocation) {
                if (currentLocation.getStatus().equals("1")) {
                    if (currentLocation.getData().size() > 0) {
                        list2.clear();
                        pbCity2.setVisibility(View.GONE);
                        list2.addAll(currentLocation.getData());
                        for (int i = 0; i < list2.size(); i++) {
                            String s = list2.get(i).getLocation();
                            city2.add(s);
                        }
                        listingAdapter(city2);


                    }
                } else {


                    showToastAlert(mActivity, currentLocation.getMessage());
                }

            }
        });
//        list1.add(getString(R.string.Ashanti));
//        list1.add(getString(R.string.Brong));
//        list1.add(getString(R.string.Central));
//        list1.add(getString(R.string.Eastern));
//        list1.add(getString(R.string.Greater));
//        list1.add(getString(R.string.Northern));
//        list1.add(getString(R.string.Upper));
//        list1.add(getString(R.string.Upper));
//        list1.add(getString(R.string.Western));
//        list1.add(getString(R.string.Volta));
        listPopupWindow2 = new ListPopupWindow(mActivity);
        listPopupWindow = new ListPopupWindow(
                mActivity);
        listPopupWindow.setAdapter(new ArrayAdapter(
                mActivity, R.layout.listpopup
                , city1));
        listPopupWindow.setAnchorView(tvPopuPSelected);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvPopuPSelected.setText(city1.get(i));
                listPopupWindow.dismiss();
            }
        });

    }

//    private void puttingDataIntoList() {
//        Ashanti.add(getString(R.string.Agogo));
//        Ashanti.add(getString(R.string.Bekwai));
//        Ashanti.add(getString(R.string.Konongo));
//        Ashanti.add(getString(R.string.Kumasi));
//        Ashanti.add(getString(R.string.Mampong));
//        Ashanti.add(getString(R.string.Mankranso));
//        Ashanti.add(getString(R.string.Obuasi));
//        Ashanti.add(getString(R.string.Ofinso));
//        Ashanti.add(getString(R.string.Tafo));
//        //2nd list
//        BrongAhafo.add(getString(R.string.Bechem));
//        BrongAhafo.add(getString(R.string.Berekum));
//        BrongAhafo.add(getString(R.string.Duayaw));
//        BrongAhafo.add(getString(R.string.Kintampo));
//        BrongAhafo.add(getString(R.string.Sunyani));
//        BrongAhafo.add(getString(R.string.Techiman));
//        BrongAhafo.add(getString(R.string.Wenchi));
////3rdlist
//        Central.add(getString(R.string.Apam));
//        Central.add(getString(R.string.Cape));
//        Central.add(getString(R.string.Dunkwa));
//        Central.add(getString(R.string.Elmina));
//        Central.add(getString(R.string.Foso));
//        Central.add(getString(R.string.Komenda));
//        Central.add(getString(R.string.Mauri));
//        Central.add(getString(R.string.Mumford));
//        Central.add(getString(R.string.Nyakrom));
//        Central.add(getString(R.string.Okitsiu));
//        Central.add(getString(R.string.Saltpond));
//        Central.add(getString(R.string.Swedru));
//        Central.add(getString(R.string.Winneba));
//        //4th list
//        Eastern.add(getString(R.string.Aburi));
//        Eastern.add(getString(R.string.Ada));
//        Eastern.add(getString(R.string.Akim));
//        Eastern.add(getString(R.string.Akropong));
//        Eastern.add(getString(R.string.Asamankese));
//        Eastern.add(getString(R.string.Begoro));
//        Eastern.add(getString(R.string.Kade));
//        Eastern.add(getString(R.string.Kibi));
//        Eastern.add(getString(R.string.Koforidua));
//        Eastern.add(getString(R.string.Mpraeso));
//        Eastern.add(getString(R.string.Nkawkaw));
//        Eastern.add(getString(R.string.Nsawam));
//        Eastern.add(getString(R.string.Oda));
//        Eastern.add(getString(R.string.Somanya));
//        Eastern.add(getString(R.string.Suhum));
//        //5th list
//        GreaterAccra.add(getString(R.string.Accra));
//        GreaterAccra.add(getString(R.string.Achimota));
//        GreaterAccra.add(getString(R.string.Ashaiman));
//        GreaterAccra.add(getString(R.string.Baatsona));
//        GreaterAccra.add(getString(R.string.Dansoman));
//        GreaterAccra.add(getString(R.string.Dodowa));
//        GreaterAccra.add(getString(R.string.Gbawe));
//        GreaterAccra.add(getString(R.string.Kasoa));
//        GreaterAccra.add(getString(R.string.Kokomlemle));
//        GreaterAccra.add(getString(R.string.Labone));
//        GreaterAccra.add(getString(R.string.Legon));
//        GreaterAccra.add(getString(R.string.Madina));
//        GreaterAccra.add(getString(R.string.Mallam));
//        GreaterAccra.add(getString(R.string.Nima));
//        GreaterAccra.add(getString(R.string.Nungua));
//        GreaterAccra.add(getString(R.string.okpoi));
//        GreaterAccra.add(getString(R.string.Sakumono));
//        GreaterAccra.add(getString(R.string.Spintex));
////6th list
//        Northern.add(getString(R.string.Kpandae));
//        Northern.add(getString(R.string.Salaga));
//        Northern.add(getString(R.string.Savelugu));
//        Northern.add(getString(R.string.Tamale));
//        Northern.add(getString(R.string.Yendi));
//        //7th list
//        UpperEast.add(getString(R.string.Bawku));
//        UpperEast.add(getString(R.string.Bolgatanga));
//        UpperEast.add(getString(R.string.Navrongo));
//        UpperEast.add(getString(R.string.Tongo));
//        //7th list
//        UpperWest.add(getString(R.string.Jirapa));
//        UpperWest.add(getString(R.string.Nandom));
//        UpperWest.add(getString(R.string.Tumu));
//        UpperWest.add(getString(R.string.Wa));
//        //8th list
//        Western.add(getString(R.string.Abosco));
//        Western.add(getString(R.string.Anom));
//        Western.add(getString(R.string.Axim));
//        Western.add(getString(R.string.Bibiani));
//        Western.add(getString(R.string.Prestea));
//        Western.add(getString(R.string.Sekondi));
//        Western.add(getString(R.string.Shama));
//        Western.add(getString(R.string.Takoradi));
//        Western.add(getString(R.string.tarkwa));
////9thlist
//        Volta.add(getString(R.string.Aflao));
//        Volta.add(getString(R.string.Anloga));
//        Volta.add(getString(R.string.Ho));
//        Volta.add(getString(R.string.Hohoe));
//        Volta.add(getString(R.string.Keta));
//        Volta.add(getString(R.string.kk));
//        Volta.add(getString(R.string.kpandu));
//        Volta.add(getString(R.string.Tema));
//    }

    @OnClick({R.id.ivBack, R.id.ivNext, R.id.tvPopuPSelected, R.id.tvPopuPSelected2})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivNext:
                if (tvPopuPSelected.getText().toString().equals(getString(R.string.selectLoc))) {
                    showErrorMessages(mActivity, getString(R.string.selectPleaseSublocation));
                } else if (tvPopuPSelected2.getText().toString().equals(getString(R.string.selectSubLoc))) {
                    showErrorMessages(mActivity, getString(R.string.selectPleaseSublocation));

                } else {
                    data.setCity1(tvPopuPSelected.getText().toString().trim());
                    data.setCity2(tvPopuPSelected2.getText().toString().trim());

                    Intent intent = new Intent(mActivity, MoneyPropertyDetailsActivity.class);
                    intent.putExtra(getString(R.string.data), data);
                    startActivity(intent);
                }
                break;
            case R.id.tvPopuPSelected:

                if (pbCity1.getVisibility() == View.GONE) {
                    if (isNetworkAvailable(mActivity) && list1.size() == 0) {
                        pbCity1.setVisibility(View.VISIBLE);
                        emptyHausViewModel.getSuLocation(data.getCityId());
                    } else if ((isNetworkAvailable(mActivity) && list1.size() > 0)) {
                        listPopupWindow.show();
                    } else {


                        showErrorMessages(mActivity, getString(R.string.internet));
                    }
                }

                break;
            case R.id.tvPopuPSelected2:
                if (pbCity2.getVisibility() == View.GONE) {
                    if (tvPopuPSelected.getText().toString().equals(getString(R.string.selectLoc))) {
                        showToastAlert(mActivity, getString(R.string.city1));
                    } else if (!tvPopuPSelected.getText().toString().equals(getString(R.string.selectLoc))) {
                        for (int j = 0; j < list1.size(); j++) {
                            if (list1.get(j).getLocation().equals(tvPopuPSelected.getText())) {
                                id = list1.get(j).getId();
                            }
                        }
                        if (isNetworkAvailable(mActivity) && list2.size() == 0) {
                            pbCity2.setVisibility(View.VISIBLE);
                            emptyHausViewModel.getSuLocation2(id);
                        } else if (isNetworkAvailable(mActivity) && list2.size() > 0) {
                            listPopupWindow2.show();
                        } else

                            showErrorMessages(mActivity, getString(R.string.internet));


                    }
                }
                break;
        }
    }

    public void listingAdapter(final List<String> cities) {

        listPopupWindow2.setAdapter(new ArrayAdapter(
                mActivity, R.layout.listpopup
                , cities));
        listPopupWindow2.setAnchorView(tvPopuPSelected2);
        listPopupWindow2.setModal(true);
        listPopupWindow2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvPopuPSelected2.setText(cities.get(i));
                listPopupWindow2.dismiss();
            }
        });
        listPopupWindow2.show();
    }
}
