package com.emptyhaus.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.emptyhaus.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CallingActivity extends BaseActivity {
    Activity mActivity = CallingActivity.this;
    @BindView(R.id.flCalling)
    FrameLayout flCalling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.flCalling)
    public void onClick() {
        Intent intent=new Intent(mActivity,CallingDetailsActivity.class);
        startActivity(intent);
        finish();
    }
}
