package com.emptyhaus.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.emptyhaus.EmptyHausApplication;
import com.emptyhaus.R;
import com.emptyhaus.model.Data;
import com.emptyhaus.model.SignUp;
import com.emptyhaus.utils.Constants;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.webservices.RestClient;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity {
    public KProgressHUD kProgressHUD;

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    public void showToastAlert(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
//
//    public void alertErrorDialog(Activity mActivity, String s) {
//        final Dialog alertDialog = new Dialog(mActivity);
//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        alertDialog.setContentView(R.layout.dialog_reminder);
//        alertDialog.setCanceledOnTouchOutside(false);
//        alertDialog.setCancelable(false);
//        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        TextView tvOk = (TextView) alertDialog.findViewById(R.id.tvOk);
//        TextView tvoops = (TextView) alertDialog.findViewById(R.id.tvOops);
//        tvoops.setText(s);
//        tvOk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//            }
//        });
//        alertDialog.show();
//
//    }

//    public void logOut(Activity activity) {
//        EmptyHausApplication.profile = null;
//        SharedPreferences preferences = EmptyHausPreference.getPreferences(activity);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.remove(EmptyHausPreference.IS_LOGIN);
//        editor.remove(EmptyHausPreference.USER_NAME);
//        editor.clear();
//        editor.commit();
//        Intent i = new Intent(activity, SplashActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        activity.startActivity(i);
//        activity.finish();
//    }

    public File setUpPhotoFile(Context mContext) throws IOException {
        File imageF = null;
        try {
            if (Environment.MEDIA_MOUNTED.equals(Environment
                    .getExternalStorageState())) {

                File storageDir = new File(
                        Environment.getExternalStorageDirectory().toString() + File.separator + "Neha").getParentFile();

                if (storageDir != null) {
                    if (!storageDir.mkdirs()) {
                        if (!storageDir.exists()) {
                            Log.d("CameraSample", "failed to create directory");
                            return null;
                        }
                    }
                }
                imageF = File.createTempFile("Img" + System.currentTimeMillis(), ".jpg", storageDir);
            } else {
                Log.v(mContext.getString(R.string.app_name),
                        "External storage is not mounted READ/WRITE.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageF;


    }

    public File saveImageToExternalStorage(Bitmap finalBitmap, Context context) {
        File file;
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                .toString();
        File myDir = new File(root + "/EmptyHaus");
        myDir.mkdirs();
        String fname = "IMG" + System.currentTimeMillis()
                + ".jpg";
        file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }


        return file;

    }

    public Bitmap getFile(String imgPath, Context mContext, int mOrientation,
                          Bitmap bMapRotate) {
        try {

            if (imgPath != null) {
                ExifInterface exif = new ExifInterface(imgPath);

                mOrientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, 1);

                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgPath, options);

                options.inSampleSize = calculateInSampleSize(options, 400, 400);
                options.inJustDecodeBounds = false;

                bMapRotate = BitmapFactory.decodeFile(imgPath, options);
                if (mOrientation == 6) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                } else if (mOrientation == 8) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(270);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                } else if (mOrientation == 3) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(180);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                }


            } else {
                Toast.makeText(
                        mContext,
                        getString(R.string.problem),
                        Toast.LENGTH_LONG).show();
            }

        } catch (OutOfMemoryError e) {
            bMapRotate = null;
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            bMapRotate = null;
            e.printStackTrace();
        }
        return bMapRotate;
    }


    public int calculateInSampleSize(BitmapFactory.Options options,
                                     int reqWidth, int reqHeight) {
        try {
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                while ((halfHeight / inSampleSize) > reqHeight
                        && (halfWidth / inSampleSize) > reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void showProgressDialog(Context context) {
        kProgressHUD = KProgressHUD.create(context).setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAnimationSpeed(Constants.PROGRESS)
                .setDimAmount(Constants.PROGRESSAMOUNT)
                .show();
    }

    //Hide Progress Dialog
    public void dismissProgressDailog() {
        kProgressHUD.dismiss();
    }

    public void showErrorMessages(Activity mActivity, String msg) {

        AlertDialog.Builder alertDialogBuilder2 = new AlertDialog.Builder(mActivity);
        LayoutInflater LayoutInflater = getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dailog_validation_layout, null);
        LinearLayout LL_ok = dialogView.findViewById(R.id.LL_ok);
        TextView textMessage = dialogView.findViewById(R.id.textMessage);
        TextView okBtn = dialogView.findViewById(R.id.okBtn);

        textMessage.setText(msg);


        alertDialogBuilder2.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder2.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();


        LL_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                String pkg = "";
                alertDialog.dismiss();
            }
        });
    }

}
