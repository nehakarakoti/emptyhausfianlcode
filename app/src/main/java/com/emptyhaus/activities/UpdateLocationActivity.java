package com.emptyhaus.activities;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.model.CurrentLocation;
import com.emptyhaus.model.SignUp;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.viewModel.EmptyHausViewModel;
import com.emptyhaus.webservices.RestClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateLocationActivity extends BaseActivity {
    Activity mActivity = UpdateLocationActivity.this;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvEnterEmail)
    TextViewBold tvEnterEmail;
    @BindView(R.id.tvPopuPSelected)
    TextViewBold tvPopuPSelected;
    @BindView(R.id.ivDropDown)
    ImageView ivDropDown;
    @BindView(R.id.rlDropDown)
    RelativeLayout rlDropDown;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    List<String> list = new ArrayList<>();
    ListPopupWindow listPopupWindow;
    @BindView(R.id.pbCities)
    ProgressBar pbCities;
EmptyHausViewModel emptyHausViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_location);
        ButterKnife.bind(this);
        emptyHausViewModel=new EmptyHausViewModel(mActivity);
        String location = EmptyHausPreference.readString(mActivity, EmptyHausPreference.LOCATION, "");
        if (!location.equals("")) {
            tvPopuPSelected.setText(location);
        }
        pbCities.setVisibility(View.VISIBLE);
        if(isNetworkAvailable(mActivity))
        emptyHausViewModel.getAllLocation();
else{
    showErrorMessages(mActivity,getString(R.string.internet));
            pbCities.setVisibility(View.GONE);}
        emptyHausViewModel.getLocationObject().observe((LifecycleOwner) mActivity, new Observer<CurrentLocation>() {
            @Override
            public void onChanged(@Nullable CurrentLocation currentLocation) {
                pbCities.setVisibility(View.GONE);
                if(currentLocation.getStatus().equals("1")){

                    for(int i=0;i<currentLocation.getData().size();i++){
                        String h=currentLocation.getData().get(i).getLocation();
                        list.add(h);
                    }
                }
                else {
                    showToastAlert(mActivity,currentLocation.getMessage());
                }
            }
        });

        listPopupWindow = new ListPopupWindow(
                mActivity);
        listPopupWindow.setAdapter(new ArrayAdapter(
                mActivity, R.layout.listpopup
                , list));
        listPopupWindow.setAnchorView(tvPopuPSelected);


        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvPopuPSelected.setText(list.get(i));
                listPopupWindow.dismiss();
            }
        });

        tvPopuPSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                listPopupWindow.show();
            }
        });
    }

    @OnClick({R.id.ivBack, R.id.ivNext})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivNext:
                if (isNetworkAvailable(mActivity))
                    api();
                else
                    showErrorMessages(mActivity, getString(R.string.internet));
                break;
        }
    }

    public void api() {
        showProgressDialog(mActivity);
        RestClient.get().updatingLocation(EmptyHausPreference.readString(mActivity, EmptyHausPreference.ID, "")
                , tvPopuPSelected.getText().toString().trim()).enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                dismissProgressDailog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals("1"))
                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.LOCATION, tvPopuPSelected.getText().toString().trim());
                    finish();
                } else
                    showErrorMessages(mActivity, response.body().getMessage());

            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String location = EmptyHausPreference.readString(mActivity, EmptyHausPreference.LOCATION, "");
        if (!location.equals("")) {
            tvPopuPSelected.setText(location);
        }
    }
}
