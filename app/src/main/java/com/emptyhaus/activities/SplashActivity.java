package com.emptyhaus.activities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewMedium;
import com.emptyhaus.utils.Constants;
import com.emptyhaus.utils.EmptyHausPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {
    Activity maActivity = SplashActivity.this;
    String existingUser = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        setSplash();

    }

    private void setSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                existingUser = EmptyHausPreference.readString(SplashActivity.this, EmptyHausPreference.USER_NAME, "");
                if (existingUser.equals("")) {
                    startActivity(new Intent(maActivity, LoginActivity.class));
                    finish();
                } else {

                    startActivity(new Intent(maActivity, MainActivity.class));
                    finish();

                }

            }
        }, Constants.SPLASH_TIME_OUT);
    }





}
