package com.emptyhaus.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emptyhaus.EmptyHausApplication;
import com.emptyhaus.R;
import com.emptyhaus.adapter.ItemAdapter;
import com.emptyhaus.dialog.ChangeFineMasterDialog;
import com.emptyhaus.dialog.RatingDaialog;
import com.emptyhaus.fonts.TextViewMedium;
import com.emptyhaus.fragment.HomeFragment;
import com.emptyhaus.model.Data;
import com.emptyhaus.utils.Constants;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.viewModel.EmptyHausViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Activity mActivity = MainActivity.this;
    EmptyHausViewModel emptyHausViewModel;
    public  static Boolean loadme=false;
    //    RecyclerView recyclerView;
//    ItemAdapter i;
TittleInterface tittleInterface;
    @BindView(R.id.llShortListed)
    LinearLayout llShortListed;
    @BindView(R.id.ivSetting)
    ImageView ivSetting;
    @BindView(R.id.ivUser)
    ImageView ivUser;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tvShort)
    TextViewMedium tvShort;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    FrameLayout container;
//    @BindView(R.id.recyclerview)
//    RecyclerView recyclerview;
//    @BindView(R.id.searchBar)
//    LinearLayout searchBar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.llHome_nav)
    LinearLayout llHomeNav;
    @BindView(R.id.llSetting)
    LinearLayout llSetting;
    @BindView(R.id.llTerms)
    LinearLayout llTerms;
    @BindView(R.id.llAbout)
    LinearLayout llAbout;
    @BindView(R.id.call)
    LinearLayout call;
    @BindView(R.id.rate)
    LinearLayout rate;
//    @BindView(R.id.tvForsale)
//    TextView tvForsale;
//    @BindView(R.id.tvForRent)
//    TextView tvForRent;
    @BindView(R.id.llSignOut)
    LinearLayout llSignOut;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    Data data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        data = (Data) getIntent().getSerializableExtra(getString(R.string.data));
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        emptyHausViewModel=new EmptyHausViewModel(mActivity);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        recyclerView = findViewById(R.id.recyclerview);
//        i = new ItemAdapter(MainActivity.this);
//        recyclerView.setAdapter(i);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, new HomeFragment())
                .commit();
      EditProfileActivity.fullName= EmptyHausPreference.readString(mActivity, EmptyHausPreference.FULL_NAME, "");

        if (!EditProfileActivity.fullName.equals("")) {
            tvUserName.setText(EditProfileActivity.fullName);
        }
        String profilePicture = EmptyHausPreference.readString(mActivity, EmptyHausPreference.PROFILE_PIC, "");
        if (!profilePicture.equals(""))
            Glide.with(mActivity).load(profilePicture).apply(new RequestOptions().circleCrop()).into(imageView);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @OnClick({R.id.llHome_nav, R.id.llSignOut, R.id.llSetting, R.id.llTerms, R.id.llAbout, R.id.call, R.id.rate, R.id.ivUser, R.id.ivSetting, R.id.llShortListed})
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.llSignOut:
//               logOut(mActivity);
            case R.id.ivUser:
MainActivity.loadme=false;
                startActivity(new Intent(mActivity, EditProfileActivity.class));
                break;
            case R.id.ivSetting:

                new ChangeFineMasterDialog(mActivity).showDialog();
                break;
            case R.id.llShortListed:
                startActivity(new Intent(mActivity, MyFavouritesActivity.class));

            case R.id.llHome_nav:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.llSetting:
                new ChangeFineMasterDialog(mActivity).showDialog();
                break;
            case R.id.llTerms:
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.llAbout:
//                String url = "http://www.dharmani.com/EmptyHaus/about-us/";
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
                drawerLayout.closeDrawer(Gravity.START);
                startActivity(new Intent(mActivity, AboutUsActivity.class));
                break;
            case R.id.call:
                drawerLayout.closeDrawer(Gravity.START);
                startActivity(new Intent(mActivity, ContactUsActivity.class));
//                String url2 = "http://www.dharmani.com/EmptyHaus/contact-us/";
//                Intent i2 = new Intent(Intent.ACTION_VIEW);
//                i2.setData(Uri.parse(url2));
//                startActivity(i2);
                break;
            case R.id.rate:
                new RatingDaialog(mActivity).showDialog();
                break;
//            case R.id.tvForRent:
//                tvForsale.setBackground(getDrawable(R.drawable.notselected));
//                tvForRent.setBackground(getDrawable(R.drawable.roundedbooundaries));
//                tvForRent.setTextColor(getResources().getColor(R.color.colorWhite));
//                tvForsale.setTextColor(getResources().getColor(R.color.colorBlue));
//                break;
//            case R.id.tvForsale:
//                tvForRent.setBackground(getDrawable(R.drawable.notselected));
//                tvForsale.setBackground(getDrawable(R.drawable.roundedbooundaries));
//                tvForsale.setTextColor(getResources().getColor(R.color.colorWhite));
//                tvForRent.setTextColor(getResources().getColor(R.color.colorBlue));
//                break;
            case R.id.llSignOut:
                emptyHausViewModel.logOut(mActivity);
                break;
        }
    }

    public void logOut(Activity activity) {
        EmptyHausApplication.profile = null;
        SharedPreferences preferences = EmptyHausPreference.getPreferences(activity);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(EmptyHausPreference.IS_LOGIN);
        editor.remove(EmptyHausPreference.USER_NAME);
        editor.remove(EmptyHausPreference.PROFILE_PIC);
        editor.clear();
        editor.commit();
        Intent i = new Intent(activity, LoginActivity.class);

        activity.startActivity(i);
        activity.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EditProfileActivity.fullName.equals("")) {
            tvUserName.setText(EditProfileActivity.fullName);
        }
//else if(michelPropertise)
    }
}
