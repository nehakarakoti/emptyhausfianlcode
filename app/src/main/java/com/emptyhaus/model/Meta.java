package com.emptyhaus.model;

public class Meta {
    private String location="";
    private String min_budget="";
    private String max_budget="";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMin_budget() {
        return min_budget;
    }

    public void setMin_budget(String min_budget) {
        this.min_budget = min_budget;
    }

    public String getMax_budget() {
        return max_budget;
    }

    public void setMax_budget(String max_budget) {
        this.max_budget = max_budget;
    }
}
