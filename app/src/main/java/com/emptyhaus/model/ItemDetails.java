package com.emptyhaus.model;

import java.util.List;

public class ItemDetails {
   HomeObject data;
    private String status = "", message = "";


    public HomeObject getData() {
        return data;
    }

    public void setData(HomeObject data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
