package com.emptyhaus.model;


import android.os.Parcel;
import android.os.Parcelable;

public class HomeObject implements Parcelable {
    public static final Creator<HomeObject> CREATOR = new Creator<HomeObject>() {
        @Override
        public HomeObject createFromParcel(Parcel in) {
            return new HomeObject(in);
        }

        @Override
        public HomeObject[] newArray(int size) {
            return new HomeObject[size];
        }
    };
    //http://emptyhaus.com/wp-content/uploads/2018/03/reality_condo_view_edited.jpg
    public String id = "";
    public String price = "";
    public String bedroom = "";
    public String bathroom = "";
    public String area = "";
    public String image = "";
    public String address = "";
    public Title title;
    public String favorite_status="";
    public Content content;

    public String getFavorite_status() {
        return favorite_status;
    }

    public void setFavorite_status(String favorite_status) {
        this.favorite_status = favorite_status;
    }

    protected HomeObject(Parcel in) {
        id = in.readString();
        price = in.readString();
        bedroom = in.readString();
        bathroom = in.readString();
        area = in.readString();
        image = in.readString();
        address = in.readString();
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBedroom() {
        return bedroom;
    }

    public void setBedroom(String bedroom) {
        this.bedroom = bedroom;
    }

    public String getBathroom() {
        return bathroom;
    }

    public void setBathroom(String bathroom) {
        this.bathroom = bathroom;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(price);
        parcel.writeString(bedroom);
        parcel.writeString(bathroom);
        parcel.writeString(area);
        parcel.writeString(image);
        parcel.writeString(address);
    }
}
