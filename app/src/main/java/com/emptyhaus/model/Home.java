package com.emptyhaus.model;

import java.util.List;

public class Home {
    List<HomeObject> data;
    private String status = "", message = "";

    public List<HomeObject> getData() {
        return data;
    }

    public void setData(List<HomeObject> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
