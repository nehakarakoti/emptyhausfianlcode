package com.emptyhaus.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emptyhaus.R;
import com.emptyhaus.fonts.TextViewMedium;
import com.emptyhaus.model.HomeObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouritesAdapter extends RecyclerView.Adapter {
    Context c;
    LayoutInflater mLayout;
    List<HomeObject> homeObjects;


    public FavouritesAdapter(Context c, List<HomeObject> homeObjects) {
        this.c = c;
        this.homeObjects = homeObjects;
        this.mLayout = LayoutInflater.from(c);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyView(LayoutInflater.from(c).inflate(R.layout.recyclerview_favouritesitem, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        MyView myView = (MyView) viewHolder;
        HomeObject homeObject = homeObjects.get(i);
        myView.tvBedRooms.setText(homeObject.getBedroom());
        myView.tvBathrroom.setText(homeObject.getBathroom());
        myView.tvAddress.setText(homeObject.getAddress());
        myView.tvPrice.setText("$"+homeObject.getPrice());
        Glide.with(c)
                .load(homeObject.getImage())
                .apply(RequestOptions.placeholderOf(R.drawable.placeholder).error(R.drawable.placeholder))
                .into(myView.ivImage);
    }

    @Override
    public int getItemCount() {
        return homeObjects.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        @BindView(R.id.ivImage)
        ImageView ivImage;

        @BindView(R.id.tvBedRooms)
        TextViewMedium tvBedRooms;
        @BindView(R.id.ivBed)
        ImageView ivBed;
        @BindView(R.id.tvBathrroom)
        TextViewMedium tvBathrroom;
        @BindView(R.id.ivvBath)
        ImageView ivvBath;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.ivPrice)
        LinearLayout ivPrice;
        @BindView(R.id.tvPrice)
        TextView tvPrice;

        public MyView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}