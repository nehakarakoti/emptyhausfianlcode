package com.emptyhaus.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emptyhaus.R;
import com.emptyhaus.activities.DetailsActivity;
import com.emptyhaus.fonts.TextViewBold;
import com.emptyhaus.fonts.TextViewRegular;
import com.emptyhaus.model.HomeObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemAdapter extends RecyclerView.Adapter {
    public Activity c;
    List<HomeObject> homeObjects;
    LayoutInflater mLayout;


    public ItemAdapter(Activity c, List<HomeObject> homeObjects) {
        this.c = c;
        this.homeObjects = homeObjects;
        mLayout = LayoutInflater.from(c);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyView(LayoutInflater.from(c).inflate(R.layout.recyclerview_item, viewGroup, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        MyView myView = (MyView) viewHolder;
        HomeObject modelObject = homeObjects.get(i);
        Glide.with(c)
                .load(modelObject.getImage())
                .apply(RequestOptions.placeholderOf(R.drawable.placeholder).error(R.drawable.placeholder))
                .into(myView.ivHouse);

        myView.tvArea.setText(modelObject.getArea().toString() + c.getResources().getString(R.string.sq));
        myView.tvBathroom.setText(modelObject.getBathroom().toString() + c.getResources().getString(R.string.bathroom));
        myView.tvBedroom.setText(modelObject.getBedroom().toString() + c.getResources().getString(R.string.bedroom));
        if (modelObject != null)
            myView.locality.setText(modelObject.getAddress());
        myView.tvName.setText(modelObject.getTitle().getRendered().toString());
    }

    @Override
    public int getItemCount() {
        return homeObjects.size();
    }


    public class MyView extends RecyclerView.ViewHolder {
        @BindView(R.id.rlPhoto)
        RelativeLayout rlPhoto;
        @BindView(R.id.ivHouse)
        ImageView ivHouse;

        @BindView(R.id.tvName)
        TextViewBold tvName;
        @BindView(R.id.ivShare)
        ImageView ivShare;
        @BindView(R.id.tvArea)
        TextView tvArea;
        @BindView(R.id.tvBedroom)
        TextView tvBedroom;
        @BindView(R.id.tvBathroom)
        TextView tvBathroom;
        @BindView(R.id.locality)
        TextView locality;

        public MyView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.rlPhoto, R.id.ivShare})
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.rlPhoto:
                    Intent intent = new Intent(c, DetailsActivity.class);
                    intent.putExtra(c.getString(R.string.comingId), homeObjects.get(getAdapterPosition()));
                    intent.putExtra(c.getString(R.string.tittleObject), homeObjects.get(getAdapterPosition()).getTitle());
                    intent.putExtra(c.getString(R.string.titleContent), homeObjects.get(getAdapterPosition()).getContent());
                   intent.putExtra(c.getString(R.string.favStatus),homeObjects.get(getAdapterPosition()).getFavorite_status());
                    c.startActivity(intent);
                    break;
                case R.id.ivShare:

                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    String str3 = homeObjects.get(getAdapterPosition()).getTitle().getRendered() + '\n' + homeObjects.get(getAdapterPosition()).getContent().getRendered();
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(Html.fromHtml(str3).toString()));
                    c.startActivity(shareIntent);
                    break;
            }


        }
    }
}
