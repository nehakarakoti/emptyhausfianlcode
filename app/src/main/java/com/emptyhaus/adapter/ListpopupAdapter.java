package com.emptyhaus.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emptyhaus.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListpopupAdapter extends RecyclerView.Adapter {
    List<String> list;
    Context c;
    LayoutInflater mLayout;
    AdapterCallback a;

    public ListpopupAdapter(Context c, List<String> list, AdapterCallback a) {
        this.c = c;
        this.list = list;
        this.mLayout = LayoutInflater.from(c);
        this.a = a;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyView(LayoutInflater.from(c).inflate(R.layout.recycler_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        String s = list.get(i);
        MyView myView = (MyView) viewHolder;
        myView.tvName.setText(s);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyView extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;

        public MyView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.tvName)
        public void onClick() {
// tvName.setText(list.get(getAdapterPosition()));
            a.onMethodCallback(true, list.get(getAdapterPosition()));
        }
    }
}