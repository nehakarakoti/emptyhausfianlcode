package com.emptyhaus.fragment;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.emptyhaus.R;
import com.emptyhaus.activities.BaseActivity;
import com.emptyhaus.activities.MainActivity;
import com.emptyhaus.adapter.ItemAdapter;
import com.emptyhaus.fonts.TextViewRegular;
import com.emptyhaus.model.Home;
import com.emptyhaus.model.HomeObject;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.viewModel.EmptyHausViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class HomeFragment extends BaseFragment {
    public static String property_type = "Sale";
    public boolean shouldScroll = false;
    List<HomeObject> homeObjectList = new ArrayList<>();
    List<HomeObject> searchList = new ArrayList<>();
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.tvForsale)
    TextViewRegular tvForsale;
    @BindView(R.id.tvForRent)
    TextViewRegular tvForRent;
    @BindView(R.id.searchBar)
    LinearLayout searchBar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    Unbinder unbinder;
    ItemAdapter i;
    EmptyHausViewModel emptyHausViewModel;
    @BindView(R.id.etSearchLocation)
    EditText etSearchLocation;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        i = new ItemAdapter(getActivity(), homeObjectList);
        recyclerview.setAdapter(i);

        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        emptyHausViewModel = new EmptyHausViewModel(getActivity());
if(isNetworkAvailable(getActivity()))
{
    MainActivity.loadme=true;
        emptyHausViewModel.homeApi(property_type, EmptyHausPreference.readString(getActivity(),EmptyHausPreference.ID,""));}
else
    showErrorMessages(getActivity(),getString(R.string.internet));
        emptyHausViewModel.getHomeData().observe(getActivity(), new Observer<Home>() {
            @Override
            public void onChanged(@Nullable Home home) {
                if (home.getStatus().equals("1")) {
                    homeObjectList.clear();
                    homeObjectList.addAll(home.getData());
                    i.notifyDataSetChanged();
  }
            }
        });
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                if (i == 0) {
                    //full
                    recyclerview.setEnabled(false);
                    shouldScroll = true;
                } else {
                    recyclerview.setEnabled(true);

                    shouldScroll = false;
                }
            }
        });
        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recycler, int dx, int dy) {

//                    if (recyclerview.canScrollVertically(-1)) {
//                        //Toast.makeText(YourActivity.this, "Last", Toast.LENGTH_LONG).show();
//                        appBar.setExpanded(false);
//                        recyclerview.setNestedScrollingEnabled(false);
//                    }
//                    else {
//                        appBar.setExpanded(true);
//                    }
            }


//                if (!recyclerview.canScrollVertically(-1)) {
//                    //Toast.makeText(YourActivity.this, "Last", Toast.LENGTH_LONG).show();
//                    appBar.setExpanded(true);
//                }
//                if (!recyclerview.canScrollVertically(1)) {
//                    //Toast.makeText(YourActivity.this, "Last", Toast.LENGTH_LONG).show();
//                    appBar.setExpanded(false);}
// If AppBar is fully expanded, revert the scroll.
//                if (shouldScroll) {
//                    recycler.scrollTo(0,0);
//                    appBar.setExpanded(true);
//                }
//                else {
//                    appBar.setExpanded(false);
//                }

        });
        etSearchLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchList.clear();
                if (editable.length() > 3) {
                    for (int i = 0; i < homeObjectList.size(); i++) {

                        if (homeObjectList.get(i).getTitle().getRendered().toLowerCase().contains(etSearchLocation.getText().toString().trim().toLowerCase())) {
                            searchList.add(homeObjectList.get(i));
                        }
                    }
                    recyclerview.setAdapter(new ItemAdapter(getActivity(), searchList));
                    recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
                } else {
                    recyclerview.setAdapter(new ItemAdapter(getActivity(), homeObjectList));
                    recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tvForsale, R.id.tvForRent})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvForRent:
                tvForsale.setBackground(getActivity().getDrawable(R.drawable.notselected));
                tvForRent.setBackground(getActivity().getDrawable(R.drawable.roundedbooundaries));
                tvForRent.setTextColor(getResources().getColor(R.color.colorWhite));
                tvForsale.setTextColor(getResources().getColor(R.color.colorBlue));
                if (isNetworkAvailable(getActivity()))

                    emptyHausViewModel.homeApi(getString(R.string.Rent), EmptyHausPreference.readString(getActivity(), EmptyHausPreference.ID, ""));
                else
                    showErrorMessages(getActivity(), getString(R.string.internet));


                break;
            case R.id.tvForsale:
                tvForRent.setBackground(getActivity().getDrawable(R.drawable.notselected));
                tvForsale.setBackground(getActivity().getDrawable(R.drawable.roundedbooundaries));
                tvForsale.setTextColor(getResources().getColor(R.color.colorWhite));
                tvForRent.setTextColor(getResources().getColor(R.color.colorBlue));
                if (isNetworkAvailable(getActivity()))

                    emptyHausViewModel.homeApi(getString(R.string.Sale), EmptyHausPreference.readString(getActivity(), EmptyHausPreference.ID, ""));
                else
                    showErrorMessages(getActivity(), getString(R.string.internet));


                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(homeObjectList.size()==0&& !MainActivity.loadme){
        if (isNetworkAvailable(getActivity()))
            emptyHausViewModel.homeApi(property_type, EmptyHausPreference.readString(getActivity(), EmptyHausPreference.ID, ""));
        else
            showErrorMessages(getActivity(), getString(R.string.internet));}
    }
}
