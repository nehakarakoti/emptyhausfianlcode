package com.emptyhaus.fonts;

import android.content.Context;
import android.graphics.Typeface;

import com.emptyhaus.R;

public class Regular {


    Context mContext;
    public static Typeface mTypeface;
    public Regular(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){
        try{
            if (mTypeface == null)
                mTypeface = Typeface.createFromAsset(mContext.getAssets(),mContext.getString(R.string.regular));
        }catch (Exception e){
            e.printStackTrace();
        }

        return mTypeface;
    }
}
