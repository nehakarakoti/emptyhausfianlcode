package com.emptyhaus.fonts;

import android.content.Context;
import android.graphics.Typeface;

import com.emptyhaus.R;



public class Medium {

    Context mContext;
    public static Typeface mTypeface;
    public Medium(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){
        try{
            if (mTypeface == null)
                mTypeface = Typeface.createFromAsset(mContext.getAssets(),mContext.getString(R.string.medium));
        }catch (Exception e){
            e.printStackTrace();
        }

        return mTypeface;
    }
}
