package com.emptyhaus.fonts;

import android.content.Context;
import android.graphics.Typeface;

import com.emptyhaus.R;



public class Bold {

    public static Typeface mTypeface;
    String path = "";
    Context mContext;

    public Bold(Context context) {
        mContext = context;
    }

    public Typeface getFontFamily() {
        path = mContext.getString(R.string.bold);
        try {
            if (mTypeface == null)
                mTypeface = Typeface.createFromAsset(mContext.getAssets(), mContext.getString(R.string.bold));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mTypeface;
    }
}
