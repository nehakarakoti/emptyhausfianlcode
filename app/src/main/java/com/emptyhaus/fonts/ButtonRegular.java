package com.emptyhaus.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;


public class ButtonRegular extends Button{
    public ButtonRegular(Context context) {
        super(context);
    }

    public ButtonRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ButtonRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ButtonRegular(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new Regular(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
