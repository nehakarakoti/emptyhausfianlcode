package com.emptyhaus.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;


public class EditTextBold extends EditText {
    public EditTextBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new Bold(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
