package com.emptyhaus.viewModel;

import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emptyhaus.EmptyHausApplication;
import com.emptyhaus.R;
import com.emptyhaus.activities.LoginActivity;
import com.emptyhaus.activities.MainActivity;
import com.emptyhaus.model.CurrentLocation;
import com.emptyhaus.model.Data;
import com.emptyhaus.model.FavList;
import com.emptyhaus.model.Favourite;
import com.emptyhaus.model.Home;
import com.emptyhaus.model.ItemDetails;
import com.emptyhaus.model.SignUp;
import com.emptyhaus.utils.Constants;
import com.emptyhaus.utils.EmptyHausPreference;
import com.emptyhaus.webservices.RestClient;
import com.kaopiz.kprogresshud.KProgressHUD;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmptyHausViewModel extends ViewModel {
    Activity context;
    MutableLiveData<CurrentLocation>locationObject=new MutableLiveData<>();
MutableLiveData<CurrentLocation> sublaoctions=new MutableLiveData<>();
    MutableLiveData<ItemDetails> itemDetailsMutableLiveData=new MutableLiveData<>();
    MutableLiveData<Data> livedata = new MutableLiveData<>();
    MutableLiveData<SignUp> signUp=new MutableLiveData<>();
    MutableLiveData<Favourite> favouriteMutableLiveData=new MutableLiveData<>();
MutableLiveData<Home> homeData=new MutableLiveData<>();
MutableLiveData<FavList> favListMutableLiveData=new MutableLiveData<>();
    public KProgressHUD kProgressHUD;

    public MutableLiveData<CurrentLocation> getSublaoctions() {
        return sublaoctions;
    }

    public void setSublaoctions(MutableLiveData<CurrentLocation> sublaoctions) {
        this.sublaoctions = sublaoctions;
    }

    public MutableLiveData<CurrentLocation> getLocationObject() {
        return locationObject;
    }

    public void setLocationObject(MutableLiveData<CurrentLocation> locationObject) {
        this.locationObject = locationObject;
    }

    public MutableLiveData<ItemDetails> getItemDetailsMutableLiveData() {
        return itemDetailsMutableLiveData;
    }

    public void setItemDetailsMutableLiveData(MutableLiveData<ItemDetails> itemDetailsMutableLiveData) {
        this.itemDetailsMutableLiveData = itemDetailsMutableLiveData;
    }

    public MutableLiveData<Data> getLivedata() {
        return livedata;
    }

    public MutableLiveData<Favourite> getFavouriteMutableLiveData() {
        return favouriteMutableLiveData;
    }

    public MutableLiveData<FavList> getFavListMutableLiveData() {
        return favListMutableLiveData;
    }

    public void setFavListMutableLiveData(MutableLiveData<FavList> favListMutableLiveData) {
        this.favListMutableLiveData = favListMutableLiveData;
    }

    public void setFavouriteMutableLiveData(MutableLiveData<Favourite> favouriteMutableLiveData) {
        this.favouriteMutableLiveData = favouriteMutableLiveData;
    }

    public EmptyHausViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<Home> getHomeData() {
        return homeData;
    }

    public void setHomeData(MutableLiveData<Home> homeData) {
        this.homeData = homeData;
    }

    public void setLivedata(MutableLiveData<Data> livedata) {
        this.livedata = livedata;
    }


    public MutableLiveData<SignUp> getSignUp() {
        return signUp;
    }

    public void setSignUp(MutableLiveData<SignUp> signUp) {
        this.signUp = signUp;
    }

    public void ItemDetails(String property_id,String user_id){
        showProgressDialog(context);
        RestClient.get().gettingItemDeatils(property_id,user_id).enqueue(new Callback<ItemDetails>() {

            @Override
            public void onResponse(Call<ItemDetails> call, Response<ItemDetails> response) {
                dismissProgressDailog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals("1")) {
             itemDetailsMutableLiveData.setValue(response.body());

                    } else {
                        showErrorMessages(context, response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<ItemDetails> call, Throwable t) {
                Log.i("exception", t.toString());
            }
        });
    }

    public void getSuLocation(String id){
        RestClient.get().getSubLocation(id).enqueue(new Callback<CurrentLocation>() {
            @Override
            public void onResponse(Call<CurrentLocation> call, Response<CurrentLocation> response) {
                if(response.isSuccessful()&& response.body()!=null){
                    locationObject.setValue(response.body());
                }

            }

            @Override
            public void onFailure(Call<CurrentLocation> call, Throwable t) {

            }
        });
    }
    public void getSuLocation2(String id){
        RestClient.get().getSubLocation(id).enqueue(new Callback<CurrentLocation>() {
            @Override
            public void onResponse(Call<CurrentLocation> call, Response<CurrentLocation> response) {
                if(response.isSuccessful()&& response.body()!=null){
                    sublaoctions.setValue(response.body());
                }

            }

            @Override
            public void onFailure(Call<CurrentLocation> call, Throwable t) {

            }
        });
    }
    public void getAllLocation(){
RestClient.get().getLocations().enqueue(new Callback<CurrentLocation>() {
    @Override
    public void onResponse(Call<CurrentLocation> call, Response<CurrentLocation> response) {
        if(response.isSuccessful()&&response.body()!=null){

            locationObject.setValue(response.body());

        }
        else
            showErrorMessages(context, response.body().getMessage());

    }

    @Override
    public void onFailure(Call<CurrentLocation> call, Throwable t) {
showErrorMessages(context,t.toString());
    }
});
    }


    public void SignUp(final Data data, final Activity mActivity) {
 showProgressDialog(mActivity);
        RestClient.get().signUp(data.getFirst_name(), data.getLast_name(), data.getEmail(), data.getPassword(), "$"+data.getMin_budget(), "$"+data.getMax_budget(), data.getLocation(), data.getCity1(), data.getCity2()).enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
               dismissProgressDailog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals("1")) {
                        signUp.setValue(response.body());

                    } else {
                        showErrorMessages(mActivity, response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Log.i("exception", t.toString());
            }
        });

    }
    public void showErrorMessages(Activity mActivity, String msg) {

        AlertDialog.Builder alertDialogBuilder2 = new AlertDialog.Builder(mActivity);
        LayoutInflater LayoutInflater =mActivity.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dailog_validation_layout, null);
        LinearLayout LL_ok = dialogView.findViewById(R.id.LL_ok);
        TextView textMessage = dialogView.findViewById(R.id.textMessage);
        TextView okBtn = dialogView.findViewById(R.id.okBtn);

        textMessage.setText(msg);


        alertDialogBuilder2.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder2.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();


        LL_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                String pkg = "";
                alertDialog.dismiss();
            }
        });
    }
    public void showProgressDialog(Context context) {
        kProgressHUD = KProgressHUD.create(context).setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAnimationSpeed(Constants.PROGRESS)
                .setDimAmount(Constants.PROGRESSAMOUNT)
                .show();
    }
    public void dismissProgressDailog() {
        kProgressHUD.dismiss();
    }
    public void signIn(final Activity mActivity, String email, String password) {
        showProgressDialog(mActivity);
        RestClient.get().logIn(email, password).enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                dismissProgressDailog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals("1")) {
                        signUp.setValue(response.body());
 }
                    else {
                        showErrorMessages(mActivity,response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {

            }
        });
    }


    public void logOut(Activity activity) {
        signUp.postValue(null);
        homeData.setValue(null);
        signUp.setValue(null);
        EmptyHausApplication.profile = null;
        SharedPreferences preferences = EmptyHausPreference.getPreferences(activity);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(EmptyHausPreference.IS_LOGIN);
        editor.remove(EmptyHausPreference.USER_NAME);
        editor.remove(EmptyHausPreference.PROFILE_PIC);
        editor.clear();
        editor.commit();
        Intent i = new Intent(activity, LoginActivity.class);

        activity.startActivity(i);
        activity.finish();
    }


    public void getEditProfileApi(Activity mActivity){

            showProgressDialog(mActivity);
            RestClient.get().gettingDetails(EmptyHausPreference.readString(mActivity, EmptyHausPreference.ID, "")).enqueue(new Callback<SignUp>() {
                @Override
                public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                    dismissProgressDailog();
                    if (response.isSuccessful() && response.body() != null) {
                        signUp.setValue(response.body());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.FULL_NAME, response.body().getData().getName());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MIN_BUDGET, response.body().getData().getMeta().getMin_budget());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.MAX_BUDGET, response.body().getData().getMeta().getMax_budget());
//                        EmptyHausPreference.writeString(mActivity, EmptyHausPreference.LOCATION, response.body().getData().getMeta().getLocation());
//                        String name = EmptyHausPreference.readString(mActivity, EmptyHausPreference.FULL_NAME, "");
//                        String email = EmptyHausPreference.readString(mActivity, EmptyHausPreference.EMAIL, "");
//                        String min = EmptyHausPreference.readString(mActivity, EmptyHausPreference.MIN_BUDGET, "");
//                        String max = EmptyHausPreference.readString(mActivity, EmptyHausPreference.MAX_BUDGET, "");
//                        if (!name.equals("")) {
//                            tvName.setText(name);
//                            etFullName.setText(name);
//                        }
//                        etEmail.setText(email);
//                        et2.setText(min);
//                        et3.setText(max);
//                        String profilePicture = EmptyHausPreference.readString(mActivity, EmptyHausPreference.PROFILE_PIC, "");
//                        if (!profilePicture.equals(""))
//                            Glide.with(mActivity).load(profilePicture).apply(new RequestOptions().circleCrop()).into(ivProfilePhoto);
//                    } else {
//                        showToastAlert(mActivity, response.body().getMessage());
//                    }
                    }
                }
                @Override
                public void onFailure(Call<SignUp> call, Throwable t) {

                }
            });





}

public void homeApi(String property_type,String u){
        showProgressDialog(context);
        RestClient.get().gettingHomeData(property_type,u).enqueue(new Callback<Home>() {
            @Override
            public void onResponse(Call<Home> call, Response<Home> response) {
                dismissProgressDailog();
                if(response.isSuccessful()&&response.body()!=null){
                    if(response.body().getStatus().equals("1"))
                    homeData.setValue(response.body());
       }
            }

            @Override
            public void onFailure(Call<Home> call, Throwable t) {

            }
        });

}
public void addingToFavourite(String id,String propertyid){
        RestClient.get().addPropertyToFavourites(id,propertyid).enqueue(new Callback<Favourite>() {
            @Override
            public void onResponse(Call<Favourite> call, Response<Favourite> response) {
                if(response.body()!=null&& response.isSuccessful()){
                favouriteMutableLiveData.setValue(response.body());
                }

            }

            @Override
            public void onFailure(Call<Favourite> call, Throwable t) {

            }
        });
}


public void favList(String userId){
        showProgressDialog(context);
        RestClient.get().favListComing(userId).enqueue(new Callback<FavList>() {
            @Override
            public void onResponse(Call<FavList> call, Response<FavList> response) {
                dismissProgressDailog();

                if(response.isSuccessful()&& response.body()!=null){
                    favListMutableLiveData.setValue(response.body());


                }
                else
                    showErrorMessages((Activity) context,response.body().getMessage());
            }

            @Override
            public void onFailure(Call<FavList> call, Throwable t) {

            }
        });

}





}
