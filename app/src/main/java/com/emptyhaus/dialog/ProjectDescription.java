package com.emptyhaus.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.TextView;

import com.emptyhaus.R;

public class ProjectDescription {
    Context context;
    Dialog dialog;
TextView tvpropertyDescription;
    public ProjectDescription(Activity context) {
        this.context = context;
    }


    public void showDialog(String s) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.projectdescription);
dialog.findViewById(R.id.btnClose).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        dialog.cancel();
    }
});
        tvpropertyDescription=dialog.findViewById(R.id.description);
        String plainText = Html.fromHtml(s).toString();

       tvpropertyDescription.setText(plainText);






        dialog.show();
        dialog.setCancelable(true);

    }
}
