package com.emptyhaus.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.emptyhaus.R;

public class RatingDaialog {

Context context;
    Dialog dialog;

    public RatingDaialog(Activity context) {
        this.context = context;
    }


    public void showDialog() {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.ratingdailog);

        dialog.show();
        dialog.setCancelable(true);

    }
}
