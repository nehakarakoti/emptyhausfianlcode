package com.emptyhaus.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import com.emptyhaus.R;
import com.emptyhaus.activities.EditProfileActivity;


/**
 * Created by SHALINI on 7/17/2018.
 */

public class ChangeFineMasterDialog {
    Context context;
    RelativeLayout rl3;
    Dialog dialog;

    public ChangeFineMasterDialog(Activity context) {
        this.context = context;
    }


    public void showDialog() {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_settings);
        rl3 = dialog.findViewById(R.id.rl3);

        rl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                context.startActivity(new Intent(context, EditProfileActivity.class));
            }
        });
        dialog.show();
        dialog.setCancelable(true);

    }

}