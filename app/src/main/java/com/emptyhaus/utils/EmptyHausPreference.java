package com.emptyhaus.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class EmptyHausPreference {
    public static final String IS_LOGIN = "is_login";
    public static final String USER_NAME = "user_name";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String SESSION_ID = "session_id";
    public static final String PASSWORD = "password";
    public static final String NICKNAME = "nickname";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String MIN_BUDGET = "min_budget";
    public static final String MAX_BUDGET = "max_budget";
    public static final String FULL_NAME = "full_name";
    public static final String LOCATION = "location";

    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;

    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean readBoolean(Context context, String key,
                                      boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();

    }

    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();

    }

    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static String getIsLogin() {
        return IS_LOGIN;
    }

    public static String getUserName() {
        return USER_NAME;
    }

    public static String getProfilePic() {
        return PROFILE_PIC;
    }

    public static String getSessionId() {
        return SESSION_ID;
    }

    public static String getID() {
        return ID;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getFirstName() {
        return FIRST_NAME;
    }

    public static String getLastName() {
        return LAST_NAME;
    }

    public static String getEMAIL() {
        return EMAIL;
    }

    public static String getPrefName() {
        return PREF_NAME;
    }

    public static int getMODE() {
        return MODE;
    }

    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}
