package com.emptyhaus.utils;

public class Constants {
//  https://dharmani.com/EmptyHaus/api/ as base url for now as client's site isn't working at present
//    public static final String BASE_URL = "http://emptyhaus.com/api/";
public static final String BASE_URL = "https://dharmani.com/EmptyHaus/api/";
    public static final String ABOUT_US="https://dharmani.com/EmptyHaus/api/about-us/";
    public static final String CONTACT_US="https://dharmani.com/EmptyHaus/api/contact-us/";
    public static final String PROFILE_IMAGE="https://secure.gravatar.com/avatar/6837837320998b51b5e1b470a5374ff3?s=96&d=mm&r=g";
    public final static int PROGRESS = 2;
    public final static int TIMER = 10000;
    public final static float PROGRESSAMOUNT = 0.5f;
    public static int SPLASH_TIME_OUT = 3000;
    public static final int PICK_IMAGE_MULTIPLE = 3;
    public static final int PICK_IMAGE_CAMERA = 2;
}
